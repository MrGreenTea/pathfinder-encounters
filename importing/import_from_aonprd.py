import dataclasses
import pathlib
from urllib import parse

import bs4
import jinja2
import requests

ALL_URL = "https://2e.aonprd.com/Monsters.aspx?Letter=All"

CREATURE_TYPE_RENAME = {
    "—": "None"
}


@dataclasses.dataclass
class Creature:
    name: str
    level: int
    size: str
    family: str
    type: str
    url: str


def get_creatures():
    request = requests.get(ALL_URL)
    request.raise_for_status()
    soup = bs4.BeautifulSoup(request.text)
    rows = soup.table.find_all("tr")[1:]  # first tr is header
    creature_types = dict()
    families = set()
    all_creatures = list()
    for row in rows:
        name, family, level, alignment, ctype, size = row.find_all("td")
        url = parse.urljoin(ALL_URL, name.a["href"])
        ctype = CREATURE_TYPE_RENAME.get(
            ctype.text, ctype.text  # rename "-" to None
        )
        cleaned_type = ctype.replace(", ", "")
        creature_types[cleaned_type] = ctype
        families.add(family.text)
        creature = Creature(name=name.text, level=int(level.text),
                            type=cleaned_type, url=url, family=family.text, size=size.text)
        all_creatures.append(creature)

    creature_types = {c: creature_types[c] for c in sorted(creature_types)}
    return creature_types, sorted(families), all_creatures


def render_template():
    environment = jinja2.Environment(loader=jinja2.FileSystemLoader(
        str(pathlib.Path(__file__).parent / "templates")))
    template = environment.get_template("Creatures.elm.j2")
    creature_types, families, all_creatures = get_creatures()

    elm_file = pathlib.Path("Creatures.elm")
    elm_file.write_text(
        template.render(
            creature_types=creature_types,
            families=[f'"{f}"' for f in families],
            creatures=all_creatures
        )
    )


if __name__ == "__main__":
    render_template()
