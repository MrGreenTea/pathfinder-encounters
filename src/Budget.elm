module Budget exposing (Threat, allThreats, calcBudget, creatureXP, sumXP, threatToString, xpToThreat)

import Creatures exposing (ChosenCreature)


type Threat
    = Trivial
    | Low
    | Moderate
    | Severe
    | Extreme


allThreats : List Threat
allThreats =
    [ Trivial, Low, Moderate, Severe, Extreme ]


threatToString : Threat -> String
threatToString threat =
    case threat of
        Trivial ->
            "Trivial"

        Low ->
            "Low"

        Moderate ->
            "Moderate"

        Severe ->
            "Severe"

        Extreme ->
            "Extreme"


calcBudget : Threat -> Int -> Int
calcBudget threat players =
    let
        base =
            case threat of
                Trivial ->
                    40

                Low ->
                    60

                Moderate ->
                    80

                Severe ->
                    120

                Extreme ->
                    160

        incr =
            case threat of
                Trivial ->
                    10

                Low ->
                    15

                Moderate ->
                    20

                Severe ->
                    30

                Extreme ->
                    40
    in
    base + incr * (players - 4)


creatureXP : Int -> Int -> Int
creatureXP partyLevel creatureLevel =
    let
        diff =
            creatureLevel - partyLevel
    in
    if diff < -4 then
        0

    else if diff == -4 then
        10

    else if diff == -3 then
        15

    else if diff == -2 then
        20

    else if diff == -1 then
        30

    else if diff == 0 then
        40

    else if diff == 1 then
        60

    else if diff == 2 then
        80

    else if diff == 3 then
        120

    else if diff == 4 then
        160

    else
        300


sumXP : List ChosenCreature -> Int -> Int
sumXP chosenCreatures partyLevel =
    List.map (\c -> c.count * creatureXP partyLevel c.creature.level) chosenCreatures |> List.sum


xpToThreat : Int -> Int -> Threat
xpToThreat players xp =
    List.sortBy (\t -> abs (xp - calcBudget t players)) allThreats |> List.head |> Maybe.withDefault Extreme
