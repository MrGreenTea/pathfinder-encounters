module Main exposing (Model, Msg(..), init, main, update, view)

import Bootstrap.Alert as Alert
import Bootstrap.Button as Button
import Bootstrap.CDN as CDN
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.Form.Select as Select
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as Col
import Bootstrap.Utilities.Flex as Flex
import Browser
import Budget
import Creatures exposing (ChosenCreature, Creature, CreatureType, Size, allCreatures, allSizes, sizeToString, typeToString)
import Dict exposing (Dict)
import Html exposing (Html, a, button, div, select, text)
import Html.Attributes exposing (class, href, placeholder, selected, value)
import Html.Events exposing (onClick, onInput)
import Table


main : Program () Model Msg
main =
    Browser.document { init = init, update = update, view = view, subscriptions = subscriptions }


type alias Model =
    { players : Int
    , partyLevel : Int
    , chosenCreatures : Dict String Int
    , allCreaturesTableState : Table.State
    , chosenCreaturesTableState : Table.State
    , filterName : String
    , filterSize : Maybe Size
    , filterType : Maybe CreatureType
    , filterFamily : Maybe String
    , filterMinLevel : Int
    , filterMaxLevel : Int
    }


type Msg
    = Add Creature
    | Remove Creature
    | SetPartyLevel Int
    | SetPlayers Int
    | SetFilterName String
    | SetFilterSize (Maybe Size)
    | SetTypeFilter (Maybe CreatureType)
    | SetFamilyFilter (Maybe String)
    | SetMinLevel Int
    | SetMaxLevel Int
    | SetAllCreaturesTableState Table.State
    | SetChosenCreaturesTableState Table.State
    | Error String


init : () -> ( Model, Cmd Msg )
init _ =
    ( { players = 4
      , partyLevel = 1
      , chosenCreatures = Dict.empty
      , filterName = ""
      , filterSize = Nothing
      , filterType = Nothing
      , filterFamily = Nothing
      , filterMinLevel = -1
      , filterMaxLevel = 25
      , allCreaturesTableState = Table.initialSort "Level"
      , chosenCreaturesTableState = Table.initialSort "Level"
      }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        new_model =
            case msg of
                Add creature ->
                    let
                        current_count =
                            Maybe.withDefault 0 (Dict.get creature.name model.chosenCreatures)
                    in
                    { model | chosenCreatures = Dict.insert creature.name (current_count + 1) model.chosenCreatures }

                Remove creature ->
                    let
                        current_count =
                            Maybe.withDefault 0 (Dict.get creature.name model.chosenCreatures)
                    in
                    if current_count <= 1 then
                        { model | chosenCreatures = Dict.remove creature.name model.chosenCreatures }

                    else
                        { model | chosenCreatures = Dict.insert creature.name (current_count - 1) model.chosenCreatures }

                SetPartyLevel level ->
                    { model | partyLevel = level, filterMinLevel = level - 4, filterMaxLevel = level + 4 }

                SetPlayers count ->
                    { model | players = count }

                SetFilterName name ->
                    { model | filterName = name }

                SetFilterSize maybeSize ->
                    { model | filterSize = maybeSize }

                SetTypeFilter maybeType ->
                    { model | filterType = maybeType }

                SetFamilyFilter maybeFamily ->
                    { model | filterFamily = maybeFamily }

                SetMinLevel level ->
                    { model | filterMinLevel = level, filterMaxLevel = clamp level 25 model.filterMaxLevel }

                SetMaxLevel level ->
                    { model | filterMaxLevel = level, filterMinLevel = clamp -1 level model.filterMinLevel }

                SetAllCreaturesTableState newState ->
                    { model | allCreaturesTableState = newState }

                SetChosenCreaturesTableState newState ->
                    { model | chosenCreaturesTableState = newState }

                Error _ ->
                    model
    in
    ( new_model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


selectToInt : (Int -> Msg) -> Select.Option Msg
selectToInt toMsg =
    let
        toInt str =
            case String.toInt str of
                Nothing ->
                    Error "Not a number."

                Just val ->
                    toMsg val
    in
    Select.onChange toInt


partyLevelSelect : Int -> Grid.Column Msg
partyLevelSelect partyLevel =
    let
        setLevel lvl =
            SetPartyLevel lvl

        mkOption lvl =
            Select.item [ selected (lvl == partyLevel), value (String.fromInt lvl) ] [ text ("Level " ++ String.fromInt lvl) ]
    in
    Grid.col [ Col.lg3 ]
        [ Form.group []
            [ Form.label [] [ text "Party Level" ]
            , Select.select [ selectToInt setLevel ] (List.map mkOption (List.range 1 20))
            ]
        ]


partyMemberSelect : Int -> Grid.Column Msg
partyMemberSelect players =
    let
        setPlayers count =
            SetPlayers count

        mkOption count =
            Select.item [ selected (count == players), value (String.fromInt count) ] [ text (String.fromInt count ++ " Players") ]
    in
    Grid.col [ Col.lg3 ]
        [ Form.group []
            [ Form.label [] [ text "Party Size" ]
            , Select.select [ selectToInt setPlayers ] (List.map mkOption (List.range 1 10))
            ]
        ]


filterSelectType : Grid.Column Msg
filterSelectType =
    let
        selectToType typeString =
            List.head <| List.filter ((==) typeString << typeToString) Creatures.allTypes

        onChange creatureType =
            SetTypeFilter creatureType

        mkOption creatureType =
            Select.item [ value (typeToString creatureType) ] [ text (typeToString creatureType) ]

        remove =
            Select.item [ value "--REMOVE-FILTER--" ] [ text "ALL" ]
    in
    Grid.col [ Col.lg2 ]
        [ Form.label [] [ text "Creature Type" ]
        , Select.select [ Select.onChange (\t -> onChange <| selectToType t) ] (remove :: List.map mkOption Creatures.allTypes)
        ]


filterSelectFamily : Grid.Column Msg
filterSelectFamily =
    let
        mkOption family =
            Select.item [] [ text family ]

        onChange v =
            if List.member v Creatures.allFamilies then
                SetFamilyFilter (Just v)

            else
                SetFamilyFilter Nothing

        remove =
            Select.item [ value "--REMOVE-FILTER--" ] [ text "ALL" ]
    in
    Grid.col [ Col.lg2 ]
        [ Form.label [] [ text "Family" ]
        , Select.select [ Select.onChange onChange ] (remove :: List.map mkOption Creatures.allFamilies)
        ]


filterLevel : Bool -> Model -> Grid.Column Msg
filterLevel isMax model =
    let
        label =
            if isMax then
                "Max Level"

            else
                "Min Level"

        defaultLvl =
            if isMax then
                25

            else
                -1

        msg lvl =
            if isMax then
                SetMaxLevel lvl

            else
                SetMinLevel lvl

        disableForLevel lvl =
            if isMax then
                lvl < model.filterMinLevel

            else
                lvl > model.filterMaxLevel

        mkOption level =
            Select.item [ value (String.fromInt level), Html.Attributes.selected (level == defaultLvl), Html.Attributes.disabled (disableForLevel level) ] [ text (String.fromInt level) ]

        onChange strLevel =
            case String.toInt strLevel of
                Just level ->
                    msg level

                Nothing ->
                    msg defaultLvl
    in
    Grid.col [ Col.lg2 ]
        [ Form.label [] [ text label ]
        , Select.select [ Select.onChange onChange ] (List.map mkOption (List.range -1 25))
        ]


filterSelectMaxLevel : Model -> Grid.Column Msg
filterSelectMaxLevel =
    filterLevel True


filterSelectMinLevel : Model -> Grid.Column Msg
filterSelectMinLevel =
    filterLevel False


filterSelectSize : Grid.Column Msg
filterSelectSize =
    let
        mkOption size =
            Select.item [ value (sizeToString size) ] [ text (sizeToString size) ]

        onChange size =
            List.head <| List.filter ((==) size << sizeToString) allSizes

        remove =
            Select.item [ value "--REMOVE-FILTER--" ] [ text "ALL" ]
    in
    Grid.col [ Col.lg2 ]
        [ Form.label [] [ text "Size" ]
        , Select.select [ Select.onChange (SetFilterSize << onChange) ] (remove :: List.map mkOption allSizes)
        ]


filterSelect : Model -> Html Msg
filterSelect model =
    Grid.row []
        [ filterSelectType
        , filterSelectFamily
        , filterSelectMinLevel model
        , filterSelectMaxLevel model
        , filterSelectSize
        ]


threatInfo : Int -> Budget.Threat -> Grid.Column Msg
threatInfo players currentThreat =
    let
        mkItem threat =
            let
                container =
                    if currentThreat == threat then
                        Html.b

                    else
                        div
            in
            Html.li [ Flex.block, Flex.justifyBetween, Flex.alignItemsCenter ]
                [ container [] [ text (Budget.threatToString threat) ]
                , div [] [ text ((String.fromInt <| Budget.calcBudget threat players) ++ " XP") ]
                ]
    in
    Grid.col [ Col.lg6 ]
        [ Html.ul [ class "list-group-flush" ] (List.map mkItem Budget.allThreats) ]


tableCustomizations : Table.Customizations data msg
tableCustomizations =
    let
        default =
            Table.defaultCustomizations
    in
    { default | tableAttrs = [ class "table table-hover" ] }


encounterTable : Table.Config ChosenCreature Msg
encounterTable =
    Table.customConfig
        { toId = .creature >> .name
        , toMsg = SetChosenCreaturesTableState
        , columns =
            [ countColumn "Count" .creature
            , linkColumn "Name" (.creature >> .source) (.creature >> .name)
            , Table.intColumn "Level" (.creature >> .level)
            ]
        , customizations = tableCustomizations
        }


chosenCreaturesList : Dict String Int -> List ChosenCreature
chosenCreaturesList chosenCreatures =
    let
        get ( k, v ) =
            case Dict.get k Creatures.allCreaturesDict of
                Maybe.Nothing ->
                    Nothing

                Maybe.Just creature ->
                    Maybe.Just { creature = creature, count = v }
    in
    Dict.toList chosenCreatures |> List.filterMap get


view : Model -> Browser.Document Msg
view model =
    { title = "Goblin Fight Club"
    , body =
        [ Grid.containerFluid []
            [ CDN.stylesheet
            , CDN.fontAwesome
            , mainContent model
            ]
        ]
    }


creaturesTable : Model -> Html Msg
creaturesTable model =
    let
        lowerQuery =
            String.toLower model.filterName

        typeFilter maybeType =
            case maybeType of
                Nothing ->
                    \_ -> True

                {- noop filter -}
                Just someType ->
                    (==) someType

        familyFilter maybeFamily =
            case maybeFamily of
                Nothing ->
                    \_ -> True

                Just someFamily ->
                    (==) someFamily

        sizeFilter maybeSize =
            case maybeSize of
                Nothing ->
                    \_ -> True

                Just someSize ->
                    (==) someSize

        checkFilter creature =
            creature.level >= model.filterMinLevel && creature.level <= model.filterMaxLevel && typeFilter model.filterType creature.creatureType && familyFilter model.filterFamily creature.family && sizeFilter model.filterSize creature.size && String.contains lowerQuery (String.toLower creature.name)

        foundCreatures =
            List.filter checkFilter allCreatures
    in
    div []
        [ Input.text [ Input.placeholder "Search by Name", Input.onInput SetFilterName ]
        , filterSelect model
        , Table.view allCreaturesTable model.allCreaturesTableState foundCreatures
        ]


mainContent : Model -> Html Msg
mainContent model =
    let
        { players, partyLevel, chosenCreatures, chosenCreaturesTableState } =
            model

        hasChosenCreatures =
            not <| Dict.isEmpty chosenCreatures

        encounters =
            if hasChosenCreatures then
                Table.view encounterTable chosenCreaturesTableState (chosenCreaturesList chosenCreatures)

            else
                Alert.simplePrimary [] [ text "Add enemies to the encounter to see the difficulty" ]

        totalXP =
            Budget.sumXP (chosenCreaturesList chosenCreatures) partyLevel

        currentThreat =
            Budget.xpToThreat players totalXP

        totalXPText =
            if hasChosenCreatures then
                [ text "Total XP: "
                , text (String.fromInt totalXP)
                , Html.b [] [ text (" (" ++ Budget.threatToString currentThreat ++ ")") ]
                ]

            else
                []
    in
    div []
        [ Grid.row []
            [ Grid.col [ Col.lg4 ]
                [ Grid.row [] [ partyMemberSelect players, partyLevelSelect partyLevel, threatInfo players currentThreat ]
                , encounters
                , Html.p [] totalXPText
                , Html.a [ href "https://gitlab.com/MrGreenTea/pathfinder-encounters", Html.Attributes.target "_blank" ] [text "Report Problems and see the source @ gitlab"]
                ]
            , Grid.col [ Col.lg8 ]
                [ creaturesTable model
                ]
            ]
        ]


addButton : Creature -> Html.Html Msg
addButton creature =
    Button.button [ Button.small, Button.success, Button.onClick (Add creature) ] [ Html.i [ class "fa fa-plus" ] [] ]


removeButton : Creature -> Html.Html Msg
removeButton creature =
    Button.button [ Button.small, Button.danger, Button.onClick (Remove creature) ] [ Html.i [ class "fa fa-minus" ] [] ]


countColumn : String -> (ChosenCreature -> Creature) -> Table.Column ChosenCreature Msg
countColumn name getter =
    Table.veryCustomColumn
        { name = name
        , viewData =
            \value ->
                Table.HtmlDetails []
                    [ Html.div [ Flex.block, Flex.justifyAround, Flex.alignItemsCenter ]
                        [ addButton (getter value)
                        , text (String.fromInt value.count)
                        , removeButton (getter value)
                        ]
                    ]
        , sorter = Table.increasingOrDecreasingBy .count
        }


addButtonColumn : String -> (a -> Creature) -> Table.Column a Msg
addButtonColumn name getter =
    Table.veryCustomColumn
        { name = name
        , viewData = \value -> Table.HtmlDetails [] [ addButton <| getter value ]
        , sorter = Table.unsortable
        }


linkColumn : String -> (a -> String) -> (a -> String) -> Table.Column a Msg
linkColumn name linkGetter textGetter =
    Table.veryCustomColumn
        { name = name
        , viewData = \value -> Table.HtmlDetails [] [ a [ href (linkGetter value) ] [ text (textGetter value) ] ]
        , sorter = Table.increasingOrDecreasingBy textGetter
        }


allCreaturesTable : Table.Config Creature Msg
allCreaturesTable =
    Table.customConfig
        { toId = .name
        , toMsg = SetAllCreaturesTableState
        , columns =
            [ addButtonColumn "" identity
            , linkColumn "Name" .source .name
            , Table.intColumn "Level" .level
            , Table.stringColumn "Family" .family
            , Table.stringColumn "Type" (typeToString << .creatureType)
            , Table.stringColumn "Size" (Creatures.sizeToString << .size)
            ]
        , customizations = tableCustomizations
        }
