module Creatures exposing
    ( ChosenCreature
    , Creature
    , CreatureType
    , Size
    , allCreatures
    , allCreaturesDict
    , allFamilies
    , allSizes
    , allTypes
    , sizeToString
    , typeToString
    )

import Dict exposing (Dict)


type CreatureType
    = Aberration
    | AberrationAstral
    | AberrationDream
    | AberrationEthereal
    | AberrationFiend
    | AberrationNegative
    | AberrationTime
    | AberrationUndead
    | Animal
    | AnimalUndead
    | Astral
    | AstralUndead
    | Beast
    | BeastDragon
    | BeastEthereal
    | BeastFey
    | BeastFiend
    | BeastHumanoid
    | Celestial
    | Construct
    | ConstructUndead
    | Dragon
    | DragonElemental
    | DragonPlant
    | DragonUndead
    | Dream
    | Elemental
    | Fey
    | FeyPlant
    | Fiend
    | FiendGiantHumanoid
    | FiendHumanoid
    | Fungus
    | FungusOoze
    | FungusPlant
    | Giant
    | GiantHumanoid
    | Humanoid
    | HumanoidPositive
    | Monitor
    | None
    | Ooze
    | Petitioner
    | Plant
    | SpiritUndead
    | Undead


type Size
    = Tiny
    | Small
    | Medium
    | Large
    | Huge
    | Gargantuan


type alias Creature =
    { name : String
    , level : Int
    , creatureType : CreatureType
    , family : String
    , size : Size
    , source : String
    }


type alias ChosenCreature =
    { creature : Creature
    , count : Int
    }


typeToString : CreatureType -> String
typeToString ct =
    case ct of
        Aberration ->
            "Aberration"

        AberrationAstral ->
            "Aberration, Astral"

        AberrationDream ->
            "Aberration, Dream"

        AberrationEthereal ->
            "Aberration, Ethereal"

        AberrationFiend ->
            "Aberration, Fiend"

        AberrationNegative ->
            "Aberration, Negative"

        AberrationTime ->
            "Aberration, Time"

        AberrationUndead ->
            "Aberration, Undead"

        Animal ->
            "Animal"

        AnimalUndead ->
            "Animal, Undead"

        Astral ->
            "Astral"

        AstralUndead ->
            "Astral, Undead"

        Beast ->
            "Beast"

        BeastDragon ->
            "Beast, Dragon"

        BeastEthereal ->
            "Beast, Ethereal"

        BeastFey ->
            "Beast, Fey"

        BeastFiend ->
            "Beast, Fiend"

        BeastHumanoid ->
            "Beast, Humanoid"

        Celestial ->
            "Celestial"

        Construct ->
            "Construct"

        ConstructUndead ->
            "Construct, Undead"

        Dragon ->
            "Dragon"

        DragonElemental ->
            "Dragon, Elemental"

        DragonPlant ->
            "Dragon, Plant"

        DragonUndead ->
            "Dragon, Undead"

        Dream ->
            "Dream"

        Elemental ->
            "Elemental"

        Fey ->
            "Fey"

        FeyPlant ->
            "Fey, Plant"

        Fiend ->
            "Fiend"

        FiendGiantHumanoid ->
            "Fiend, Giant, Humanoid"

        FiendHumanoid ->
            "Fiend, Humanoid"

        Fungus ->
            "Fungus"

        FungusOoze ->
            "Fungus, Ooze"

        FungusPlant ->
            "Fungus, Plant"

        Giant ->
            "Giant"

        GiantHumanoid ->
            "Giant, Humanoid"

        Humanoid ->
            "Humanoid"

        HumanoidPositive ->
            "Humanoid, Positive"

        Monitor ->
            "Monitor"

        None ->
            "None"

        Ooze ->
            "Ooze"

        Petitioner ->
            "Petitioner"

        Plant ->
            "Plant"

        SpiritUndead ->
            "Spirit, Undead"

        Undead ->
            "Undead"


sizeToString : Size -> String
sizeToString size =
    case size of
        Tiny ->
            "Tiny"

        Small ->
            "Small"

        Medium ->
            "Medium"

        Large ->
            "Large"

        Huge ->
            "Huge"

        Gargantuan ->
            "Gargantuan"


allSizes : List Size
allSizes =
    [ Tiny
    , Small
    , Medium
    , Large
    , Huge
    , Gargantuan
    ]


allTypes : List CreatureType
allTypes =
    [ Aberration
    , AberrationAstral
    , AberrationDream
    , AberrationEthereal
    , AberrationFiend
    , AberrationNegative
    , AberrationTime
    , AberrationUndead
    , Animal
    , AnimalUndead
    , Astral
    , AstralUndead
    , Beast
    , BeastDragon
    , BeastEthereal
    , BeastFey
    , BeastFiend
    , BeastHumanoid
    , Celestial
    , Construct
    , ConstructUndead
    , Dragon
    , DragonElemental
    , DragonPlant
    , DragonUndead
    , Dream
    , Elemental
    , Fey
    , FeyPlant
    , Fiend
    , FiendGiantHumanoid
    , FiendHumanoid
    , Fungus
    , FungusOoze
    , FungusPlant
    , Giant
    , GiantHumanoid
    , Humanoid
    , HumanoidPositive
    , Monitor
    , None
    , Ooze
    , Petitioner
    , Plant
    , SpiritUndead
    , Undead
    ]


allFamilies : List String
allFamilies =
    [ "Aeon"
    , "Alghollthu"
    , "Aluum"
    , "Anadi"
    , "Angel"
    , "Animated Object"
    , "Ankhrav"
    , "Ant"
    , "Ape"
    , "Arboreal"
    , "Archon"
    , "Aukashungi"
    , "Azata"
    , "Badger"
    , "Barghest"
    , "Basilisk"
    , "Bat"
    , "Bear"
    , "Beetle"
    , "Biloko"
    , "Blightburn Genies"
    , "Boar"
    , "Bogeyman"
    , "Boggard"
    , "Bone Skipper"
    , "Brughadatch"
    , "Bugbear"
    , "Caligni"
    , "Cat"
    , "Catfolk"
    , "Cave Worm"
    , "Centipede"
    , "Changeling"
    , "Charau-ka"
    , "Cockroach"
    , "Couatl"
    , "Crab"
    , "Crawling Hand"
    , "Crocodile"
    , "Cyclops"
    , "Daemon"
    , "Deadly Flora"
    , "Deep Gnome"
    , "Demon"
    , "Dero"
    , "Devil"
    , "Dhampir"
    , "Dinosaur"
    , "Dog"
    , "Dolphin"
    , "Dragon, Black"
    , "Dragon, Blue"
    , "Dragon, Brass"
    , "Dragon, Brine"
    , "Dragon, Bronze"
    , "Dragon, Cloud"
    , "Dragon, Copper"
    , "Dragon, Crystal"
    , "Dragon, Gold"
    , "Dragon, Green"
    , "Dragon, Magma"
    , "Dragon, Red"
    , "Dragon, Silver"
    , "Dragon, Umbral"
    , "Dragon, White"
    , "Dragonfly"
    , "Drake"
    , "Drow"
    , "Duergar"
    , "Eagle"
    , "Eel"
    , "Elemental, Air"
    , "Elemental, Earth"
    , "Elemental, Fire"
    , "Elemental, Mephit"
    , "Elemental, Water"
    , "Elephant"
    , "Fetchling"
    , "Flea"
    , "Fleshwarp"
    , "Fly"
    , "Flytrap"
    , "Frog"
    , "Genie"
    , "Geniekin"
    , "Ghost"
    , "Ghoul"
    , "Giant"
    , "Gnoll"
    , "Goblin"
    , "Golem"
    , "Gremlin"
    , "Grim Reaper"
    , "Grippli"
    , "Hag"
    , "Hell Hound"
    , "Herecite"
    , "Hippocampus"
    , "Hippopotamus"
    , "Hobgoblin"
    , "Horse"
    , "Hyena"
    , "Jellyfish"
    , "Kobold"
    , "Lamia"
    , "Leech"
    , "Leshy"
    , "Lich"
    , "Linnorm"
    , "Lizard"
    , "Lizardfolk"
    , "Mantis, Giant"
    , "Merfolk"
    , "Mosquito"
    , "Mummy"
    , "Naga"
    , "Nightmare"
    , "Nymph"
    , "Octopus"
    , "Ogre"
    , "Oni"
    , "Ooze"
    , "Orc"
    , "Planar Scion"
    , "Protean"
    , "Psychopomp"
    , "Pterosaur"
    , "Qlippoth"
    , "Rakshasa"
    , "Rat"
    , "Ratfolk"
    , "Raven"
    , "Ravener"
    , "Ray"
    , "Rhinoceros"
    , "Sahkil"
    , "Saurian"
    , "Scorpion"
    , "Sea Devil"
    , "Serpentfolk"
    , "Shadow"
    , "Shark"
    , "Shoony"
    , "Skeleton"
    , "Snake"
    , "Solifugid"
    , "Spawn of Rovagug"
    , "Spider"
    , "Sportlebore"
    , "Spriggan"
    , "Sprite"
    , "Squid"
    , "Tick"
    , "Titan"
    , "Toad"
    , "Troll"
    , "Turtle"
    , "Urdefhan"
    , "Vampire"
    , "Vampire, Vrykolakas"
    , "Velstrac"
    , "Visitant"
    , "Warg"
    , "Wasp"
    , "Werecreature"
    , "Wight"
    , "Wolf"
    , "Wolverine"
    , "Worm That Walks"
    , "Wraith"
    , "Wyrmwraith"
    , "Xulgath"
    , "Yellow Musk Creeper"
    , "Zombie"
    , "—"
    ]


allCreatures : List Creature
allCreatures =
    [ { name = "Aapoph Serpentfolk", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=799", family = "Serpentfolk", size = Medium }
    , { name = "Aasimar", level = 5, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=333", family = "Planar Scion", size = Medium }
    , { name = "Abrikandilu (Wrecker Demon)", level = 4, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=499", family = "Demon", size = Medium }
    , { name = "Adamantine Golem", level = 18, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=243", family = "Golem", size = Huge }
    , { name = "Adult Black Dragon", level = 11, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=128", family = "Dragon, Black", size = Large }
    , { name = "Adult Blue Dragon", level = 13, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=131", family = "Dragon, Blue", size = Huge }
    , { name = "Adult Brass Dragon", level = 11, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=143", family = "Dragon, Brass", size = Large }
    , { name = "Adult Brine Dragon", level = 12, creatureType = DragonElemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=622", family = "Dragon, Brine", size = Huge }
    , { name = "Adult Bronze Dragon", level = 13, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=146", family = "Dragon, Bronze", size = Huge }
    , { name = "Adult Cloud Dragon", level = 14, creatureType = DragonElemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=625", family = "Dragon, Cloud", size = Huge }
    , { name = "Adult Copper Dragon", level = 12, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=149", family = "Dragon, Copper", size = Large }
    , { name = "Adult Crystal Dragon", level = 11, creatureType = DragonElemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=628", family = "Dragon, Crystal", size = Huge }
    , { name = "Adult Gold Dragon", level = 15, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=152", family = "Dragon, Gold", size = Huge }
    , { name = "Adult Green Dragon", level = 12, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=134", family = "Dragon, Green", size = Huge }
    , { name = "Adult Magma Dragon", level = 13, creatureType = DragonElemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=632", family = "Dragon, Magma", size = Huge }
    , { name = "Adult Red Dragon", level = 14, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=137", family = "Dragon, Red", size = Huge }
    , { name = "Adult Silver Dragon", level = 14, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=155", family = "Dragon, Silver", size = Huge }
    , { name = "Adult Umbral Dragon", level = 15, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=635", family = "Dragon, Umbral", size = Huge }
    , { name = "Adult White Dragon", level = 10, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=140", family = "Dragon, White", size = Large }
    , { name = "Agradaemon (Conflagration Daemon)", level = 19, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=1018", family = "Daemon", size = Gargantuan }
    , { name = "Ahuizotl", level = 6, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=540", family = "—", size = Large }
    , { name = "Ahvothian", level = 7, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=983", family = "—", size = Medium }
    , { name = "Air Mephit", level = 1, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=192", family = "Elemental, Mephit", size = Small }
    , { name = "Aiudara Wraith", level = 18, creatureType = AstralUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=488", family = "Wraith", size = Medium }
    , { name = "Akata", level = 1, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=541", family = "—", size = Medium }
    , { name = "Akizendri", level = 3, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=764", family = "Protean", size = Small }
    , { name = "Alchemical Golem", level = 9, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=239", family = "Golem", size = Large }
    , { name = "Alghollthu Master (Aboleth)", level = 7, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=2", family = "Alghollthu", size = Huge }
    , { name = "Aluum Enforcer", level = 10, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=476", family = "Aluum", size = Large }
    , { name = "Ammut", level = 18, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=872", family = "—", size = Huge }
    , { name = "Amoeba Swarm", level = 1, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=750", family = "Ooze", size = Large }
    , { name = "Anadi Elder", level = 6, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=432", family = "Anadi", size = Medium }
    , { name = "Anadi Hunter", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=430", family = "Anadi", size = Medium }
    , { name = "Anadi Sage", level = 4, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=431", family = "Anadi", size = Medium }
    , { name = "Anancus", level = 8, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=667", family = "Elephant", size = Huge }
    , { name = "Ancient Black Dragon", level = 16, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=129", family = "Dragon, Black", size = Huge }
    , { name = "Ancient Blue Dragon", level = 18, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=132", family = "Dragon, Blue", size = Huge }
    , { name = "Ancient Brass Dragon", level = 16, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=144", family = "Dragon, Brass", size = Huge }
    , { name = "Ancient Brine Dragon", level = 17, creatureType = DragonElemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=623", family = "Dragon, Brine", size = Gargantuan }
    , { name = "Ancient Bronze Dragon", level = 18, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=147", family = "Dragon, Bronze", size = Gargantuan }
    , { name = "Ancient Cloud Dragon", level = 19, creatureType = DragonElemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=626", family = "Dragon, Cloud", size = Gargantuan }
    , { name = "Ancient Copper Dragon", level = 17, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=150", family = "Dragon, Copper", size = Huge }
    , { name = "Ancient Crystal Dragon", level = 16, creatureType = DragonElemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=629", family = "Dragon, Crystal", size = Gargantuan }
    , { name = "Ancient Gold Dragon", level = 20, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=153", family = "Dragon, Gold", size = Gargantuan }
    , { name = "Ancient Green Dragon", level = 17, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=135", family = "Dragon, Green", size = Gargantuan }
    , { name = "Ancient Magma Dragon", level = 18, creatureType = DragonElemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=633", family = "Dragon, Magma", size = Gargantuan }
    , { name = "Ancient Red Dragon", level = 19, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=138", family = "Dragon, Red", size = Huge }
    , { name = "Ancient Silver Dragon", level = 19, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=156", family = "Dragon, Silver", size = Gargantuan }
    , { name = "Ancient Umbral Dragon", level = 20, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=636", family = "Dragon, Umbral", size = Gargantuan }
    , { name = "Ancient White Dragon", level = 15, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=141", family = "Dragon, White", size = Huge }
    , { name = "Animate Dream", level = 8, creatureType = Dream, source = "https://2e.aonprd.com/Monsters.aspx?ID=546", family = "—", size = Medium }
    , { name = "Animated Armor", level = 2, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=19", family = "Animated Object", size = Medium }
    , { name = "Animated Broom", level = -1, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=18", family = "Animated Object", size = Small }
    , { name = "Animated Statue", level = 3, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=20", family = "Animated Object", size = Medium }
    , { name = "Ankhrav", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=23", family = "Ankhrav", size = Large }
    , { name = "Ankou", level = 14, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=547", family = "—", size = Large }
    , { name = "Ankylosaurus", level = 6, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=119", family = "Dinosaur", size = Huge }
    , { name = "Annis Hag", level = 6, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=256", family = "Hag", size = Large }
    , { name = "Aolaz", level = 18, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=550", family = "—", size = Gargantuan }
    , { name = "Ararda", level = 18, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=874", family = "Blightburn Genies", size = Large }
    , { name = "Arbiter", level = 1, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=6", family = "Aeon", size = Tiny }
    , { name = "Arboreal Regent", level = 8, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=29", family = "Arboreal", size = Huge }
    , { name = "Arboreal Warden", level = 4, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=27", family = "Arboreal", size = Large }
    , { name = "Army Ant Swarm", level = 5, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=549", family = "Ant", size = Large }
    , { name = "Asanbosam", level = 6, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=439", family = "—", size = Large }
    , { name = "Assassin Vine", level = 3, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=555", family = "—", size = Large }
    , { name = "Astradaemon (Void Daemon)", level = 16, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=91", family = "Daemon", size = Large }
    , { name = "Astral Deva (Emissary Angel)", level = 14, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=16", family = "Angel", size = Medium }
    , { name = "Athach", level = 12, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=556", family = "—", size = Huge }
    , { name = "Attic Whisperer", level = 4, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=557", family = "—", size = Small }
    , { name = "Augnagar", level = 14, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=776", family = "Qlippoth", size = Huge }
    , { name = "Augur", level = 1, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=847", family = "Velstrac", size = Tiny }
    , { name = "Aurumvorax", level = 9, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=558", family = "—", size = Small }
    , { name = "Avarek", level = 8, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=998", family = "—", size = Small }
    , { name = "Awakened Tree", level = 6, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=28", family = "Arboreal", size = Huge }
    , { name = "Axiomite", level = 8, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=7", family = "Aeon", size = Medium }
    , { name = "Azure Worm", level = 15, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=74", family = "Cave Worm", size = Gargantuan }
    , { name = "Azuretzi", level = 5, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=765", family = "Protean", size = Small }
    , { name = "Baatamidar", level = 21, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=1015", family = "—", size = Medium }
    , { name = "Babau (Blood Demon)", level = 6, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=598", family = "Demon", size = Medium }
    , { name = "Badger", level = 0, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=561", family = "Badger", size = Small }
    , { name = "Balisse (Confessor Angel)", level = 8, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=15", family = "Angel", size = Medium }
    , { name = "Ball Python", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=380", family = "Snake", size = Medium }
    , { name = "Balor (Fire Demon)", level = 20, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=104", family = "Demon", size = Large }
    , { name = "Banshee", level = 17, creatureType = SpiritUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=39", family = "—", size = Medium }
    , { name = "Baobhan Sith", level = 6, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=563", family = "—", size = Medium }
    , { name = "Baomal", level = 20, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=40", family = "—", size = Gargantuan }
    , { name = "Barbazu (Bearded Devil)", level = 5, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=110", family = "Devil", size = Medium }
    , { name = "Barghest", level = 4, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=41", family = "Barghest", size = Medium }
    , { name = "Barnacle Ghoul", level = 9, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=1001", family = "Ghoul", size = Medium }
    , { name = "Basidirond", level = 5, creatureType = Fungus, source = "https://2e.aonprd.com/Monsters.aspx?ID=564", family = "—", size = Medium }
    , { name = "Basilisk", level = 5, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=44", family = "Basilisk", size = Medium }
    , { name = "Bastion Archon", level = 20, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=554", family = "Archon", size = Huge }
    , { name = "Bebilith", level = 10, creatureType = BeastFiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=568", family = "—", size = Huge }
    , { name = "Behemoth Hippopotamus", level = 10, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=700", family = "Hippopotamus", size = Huge }
    , { name = "Behir", level = 8, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=569", family = "—", size = Huge }
    , { name = "Belker", level = 6, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=648", family = "Elemental, Air", size = Large }
    , { name = "Bida", level = 8, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=440", family = "—", size = Huge }
    , { name = "Biloko Veteran", level = 4, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=442", family = "Biloko", size = Small }
    , { name = "Biloko Warrior", level = 1, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=441", family = "Biloko", size = Small }
    , { name = "Binumir", level = 3, creatureType = SpiritUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=985", family = "—", size = Medium }
    , { name = "Black Bear", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=566", family = "Bear", size = Large }
    , { name = "Black Pudding", level = 7, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=323", family = "Ooze", size = Huge }
    , { name = "Black Scorpion", level = 15, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=796", family = "Scorpion", size = Gargantuan }
    , { name = "Blindheim", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=570", family = "—", size = Small }
    , { name = "Blink Dog", level = 2, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=571", family = "—", size = Medium }
    , { name = "Blizzardborn", level = 6, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=665", family = "Elemental, Water", size = Medium }
    , { name = "Blodeuwedd", level = 6, creatureType = FeyPlant, source = "https://2e.aonprd.com/Monsters.aspx?ID=572", family = "—", size = Medium }
    , { name = "Blood Boar", level = 6, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=454", family = "—", size = Medium }
    , { name = "Blood Ooze", level = 4, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=429", family = "Ooze", size = Large }
    , { name = "Bloodlash Bush", level = 2, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=427", family = "Deadly Flora", size = Small }
    , { name = "Bloodseeker", level = -1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=51", family = "—", size = Tiny }
    , { name = "Blue-Ringed Octopus", level = 0, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=744", family = "Octopus", size = Tiny }
    , { name = "Boar", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=52", family = "Boar", size = Medium }
    , { name = "Bodak", level = 8, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=573", family = "—", size = Medium }
    , { name = "Bog Mummy", level = 5, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=735", family = "Mummy", size = Medium }
    , { name = "Bog Strider", level = 2, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=574", family = "—", size = Medium }
    , { name = "Bogey", level = 3, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=507", family = "Bogeyman", size = Small }
    , { name = "Bogeyman", level = 10, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=509", family = "Bogeyman", size = Medium }
    , { name = "Boggard Scout", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=54", family = "Boggard", size = Medium }
    , { name = "Boggard Swampseer", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=56", family = "Boggard", size = Medium }
    , { name = "Boggard Warrior", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=55", family = "Boggard", size = Medium }
    , { name = "Bone Croupier", level = 5, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=498", family = "—", size = Medium }
    , { name = "Bone Prophet", level = 8, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=801", family = "Serpentfolk", size = Medium }
    , { name = "Bone Skipper Swarm", level = 6, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=989", family = "Bone Skipper", size = Large }
    , { name = "Bottlenose Dolphin", level = 0, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=618", family = "Dolphin", size = Medium }
    , { name = "Brain Collector", level = 8, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=57", family = "—", size = Large }
    , { name = "Bralani (Wind Azata)", level = 6, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=559", family = "Azata", size = Medium }
    , { name = "Bregdi", level = 9, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=999", family = "—", size = Large }
    , { name = "Brine Shark", level = 3, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=196", family = "Elemental, Water", size = Medium }
    , { name = "Brontosaurus", level = 10, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=122", family = "Dinosaur", size = Gargantuan }
    , { name = "Brood Leech Swarm", level = 4, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=712", family = "Leech", size = Large }
    , { name = "Brownie", level = 1, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=575", family = "—", size = Tiny }
    , { name = "Brughadatch (Midlife Brughadatch)", level = 10, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=528", family = "Brughadatch", size = Medium }
    , { name = "Bugaboo", level = 6, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=508", family = "Bogeyman", size = Medium }
    , { name = "Bugbear Thug", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=58", family = "Bugbear", size = Medium }
    , { name = "Bugbear Tormentor", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=59", family = "Bugbear", size = Medium }
    , { name = "Bugul Noz", level = 12, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=516", family = "—", size = Medium }
    , { name = "Bulette", level = 8, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=61", family = "—", size = Huge }
    , { name = "Bunyip", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=62", family = "—", size = Medium }
    , { name = "Bythos", level = 16, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=539", family = "Aeon", size = Large }
    , { name = "Cacodaemon (Harvester Daemon)", level = 1, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=88", family = "Daemon", size = Tiny }
    , { name = "Cairn Linnorm", level = 18, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=722", family = "Linnorm", size = Gargantuan }
    , { name = "Cairn Wight", level = 4, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=859", family = "Wight", size = Medium }
    , { name = "Calathgar", level = 4, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=576", family = "—", size = Small }
    , { name = "Caligni Creeper", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=65", family = "Caligni", size = Small }
    , { name = "Caligni Dancer", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=64", family = "Caligni", size = Small }
    , { name = "Caligni Slayer", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=577", family = "Caligni", size = Small }
    , { name = "Caligni Stalker", level = 4, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=66", family = "Caligni", size = Medium }
    , { name = "Calikang", level = 12, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=478", family = "—", size = Large }
    , { name = "Camarach", level = 17, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=1016", family = "—", size = Gargantuan }
    , { name = "Carbuncle", level = 1, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=578", family = "—", size = Tiny }
    , { name = "Carnivorous Blob", level = 13, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=755", family = "Ooze", size = Gargantuan }
    , { name = "Carnivorous Crystal", level = 11, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=464", family = "—", size = Medium }
    , { name = "Carrion Golem", level = 4, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=682", family = "Golem", size = Medium }
    , { name = "Cassisian (Archive Angel)", level = 1, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=13", family = "Angel", size = Tiny }
    , { name = "Cat Sith", level = 6, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=517", family = "—", size = Tiny }
    , { name = "Catfolk Pouncer", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=71", family = "Catfolk", size = Medium }
    , { name = "Catoblepas", level = 12, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=579", family = "—", size = Large }
    , { name = "Catrina", level = 5, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=768", family = "Psychopomp", size = Medium }
    , { name = "Cauthooj", level = 12, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=72", family = "—", size = Medium }
    , { name = "Cave Bear", level = 6, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=48", family = "Bear", size = Large }
    , { name = "Cave Fisher", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=580", family = "—", size = Medium }
    , { name = "Cave Scorpion", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=795", family = "Scorpion", size = Medium }
    , { name = "Cavern Troll", level = 6, creatureType = Giant, source = "https://2e.aonprd.com/Monsters.aspx?ID=832", family = "Troll", size = Large }
    , { name = "Centaur", level = 3, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=76", family = "—", size = Large }
    , { name = "Centipede Swarm", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=78", family = "Centipede", size = Large }
    , { name = "Ceustodaemon (Guardian Daemon)", level = 6, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=89", family = "Daemon", size = Large }
    , { name = "Changeling Exile", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=79", family = "Changeling", size = Medium }
    , { name = "Charau-ka Acolyte of Angazhan", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=445", family = "Charau-ka", size = Small }
    , { name = "Charau-ka Butcher", level = 6, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=446", family = "Charau-ka", size = Small }
    , { name = "Charau-ka Warrior", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=444", family = "Charau-ka", size = Small }
    , { name = "Chernobue", level = 12, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=775", family = "Qlippoth", size = Large }
    , { name = "Chimera", level = 8, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=80", family = "—", size = Large }
    , { name = "Chimpanzee Visitant", level = 3, creatureType = AnimalUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=512", family = "Visitant", size = Small }
    , { name = "Choker", level = 2, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=583", family = "—", size = Small }
    , { name = "Choral (Choir Angel)", level = 6, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=14", family = "Angel", size = Small }
    , { name = "Chupacabra", level = 3, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=584", family = "—", size = Small }
    , { name = "Chuul", level = 7, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=81", family = "—", size = Large }
    , { name = "Cinder Rat", level = 3, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=187", family = "Elemental, Fire", size = Small }
    , { name = "Clay Golem", level = 10, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=240", family = "Golem", size = Large }
    , { name = "Cloaker", level = 5, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=82", family = "—", size = Large }
    , { name = "Clockwork Amalgam", level = 20, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=1008", family = "—", size = Large }
    , { name = "Clockwork Assassin", level = 13, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=1004", family = "—", size = Medium }
    , { name = "Cloud Giant", level = 11, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=224", family = "Giant", size = Huge }
    , { name = "Cobbleswarm", level = 2, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=986", family = "—", size = Large }
    , { name = "Cockatrice", level = 3, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=83", family = "—", size = Small }
    , { name = "Cockroach Swarm", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=586", family = "Cockroach", size = Small }
    , { name = "Coil Spy", level = 4, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=800", family = "Serpentfolk", size = Medium }
    , { name = "Compsognathus", level = -1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=613", family = "Dinosaur", size = Tiny }
    , { name = "Cornugon (Horned Devil)", level = 16, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=611", family = "Devil", size = Large }
    , { name = "Counteflora", level = 10, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=518", family = "—", size = Large }
    , { name = "Crag Linnorm", level = 14, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=284", family = "Linnorm", size = Gargantuan }
    , { name = "Crawling Hand", level = -1, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=589", family = "Crawling Hand", size = Tiny }
    , { name = "Crimson Worm", level = 18, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=75", family = "Cave Worm", size = Gargantuan }
    , { name = "Crocodile", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=84", family = "Crocodile", size = Large }
    , { name = "Crucidaemon (Torture Daemon)", level = 15, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=479", family = "Daemon", size = Medium }
    , { name = "Cu Sith", level = 7, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=519", family = "—", size = Large }
    , { name = "Culdewen", level = 7, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=591", family = "—", size = Small }
    , { name = "Cyclops", level = 5, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=86", family = "Cyclops", size = Large }
    , { name = "Cythnigot", level = 1, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=772", family = "Qlippoth", size = Tiny }
    , { name = "Daeodon", level = 4, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=53", family = "Boar", size = Large }
    , { name = "Dalos", level = 13, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=465", family = "—", size = Huge }
    , { name = "Dandasuka", level = 5, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=344", family = "Rakshasa", size = Small }
    , { name = "Dark Naga", level = 7, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=306", family = "Naga", size = Large }
    , { name = "Deadly Mantis", level = 11, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=296", family = "Mantis, Giant", size = Gargantuan }
    , { name = "Deculi", level = 12, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=466", family = "—", size = Large }
    , { name = "Deep Gnome Rockwarden", level = 5, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=96", family = "Deep Gnome", size = Small }
    , { name = "Deep Gnome Scout", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=93", family = "Deep Gnome", size = Small }
    , { name = "Deep Gnome Warrior", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=94", family = "Deep Gnome", size = Small }
    , { name = "Deghuun (Children Of Mhar)", level = 18, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=975", family = "—", size = Huge }
    , { name = "Deinonychus", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=118", family = "Dinosaur", size = Medium }
    , { name = "Deinosuchus", level = 9, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=85", family = "Crocodile", size = Huge }
    , { name = "Demilich", level = 15, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=283", family = "Lich", size = Tiny }
    , { name = "Denizen of Leng", level = 8, creatureType = AberrationDream, source = "https://2e.aonprd.com/Monsters.aspx?ID=604", family = "—", size = Medium }
    , { name = "Derghodaemon (Ravager Daemon)", level = 12, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=594", family = "Daemon", size = Large }
    , { name = "Dero Magister", level = 5, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=107", family = "Dero", size = Small }
    , { name = "Dero Stalker", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=105", family = "Dero", size = Small }
    , { name = "Dero Strangler", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=106", family = "Dero", size = Small }
    , { name = "Desert Drake", level = 8, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=164", family = "Drake", size = Large }
    , { name = "Destrachan", level = 8, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=605", family = "—", size = Large }
    , { name = "Devourer", level = 11, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=612", family = "—", size = Large }
    , { name = "Dezullon", level = 10, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=115", family = "—", size = Medium }
    , { name = "Dhampir Wizard", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=116", family = "Dhampir", size = Medium }
    , { name = "Dig-Widget", level = 5, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=771", family = "—", size = Small }
    , { name = "Dire Wolf", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=416", family = "Wolf", size = Large }
    , { name = "Djinni", level = 5, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=212", family = "Genie", size = Large }
    , { name = "Doblagub (Elder Brughadatch)", level = 13, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=529", family = "Brughadatch", size = Large }
    , { name = "Doorwarden", level = 5, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=433", family = "—", size = Large }
    , { name = "Doppelganger", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=126", family = "—", size = Medium }
    , { name = "Doprillu", level = 14, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=620", family = "—", size = Medium }
    , { name = "Dracolisk", level = 9, creatureType = BeastDragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=565", family = "Basilisk", size = Large }
    , { name = "Dragon Turtle", level = 9, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=157", family = "—", size = Huge }
    , { name = "Dragonscarred Dead", level = 13, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=472", family = "—", size = Medium }
    , { name = "Dragonshard Guardian", level = 22, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=489", family = "—", size = Large }
    , { name = "Drainberry Bush", level = 7, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=639", family = "—", size = Large }
    , { name = "Drakauthix", level = 9, creatureType = Fungus, source = "https://2e.aonprd.com/Monsters.aspx?ID=158", family = "—", size = Huge }
    , { name = "Draugr", level = 2, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=642", family = "—", size = Medium }
    , { name = "Dread Wraith", level = 9, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=865", family = "Wraith", size = Large }
    , { name = "Dreadsong Dancer", level = 8, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=991", family = "—", size = Medium }
    , { name = "Dream Spider", level = 0, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=813", family = "Spider", size = Small }
    , { name = "Drider", level = 6, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=207", family = "Fleshwarp", size = Large }
    , { name = "Drow Fighter", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=165", family = "Drow", size = Medium }
    , { name = "Drow Priestess", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=167", family = "Drow", size = Medium }
    , { name = "Drow Rogue", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=166", family = "Drow", size = Medium }
    , { name = "Dryad Queen (Hamadryad)", level = 13, creatureType = FeyPlant, source = "https://2e.aonprd.com/Monsters.aspx?ID=314", family = "Nymph", size = Medium }
    , { name = "Dryad", level = 3, creatureType = FeyPlant, source = "https://2e.aonprd.com/Monsters.aspx?ID=312", family = "Nymph", size = Medium }
    , { name = "Duergar Bombardier", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=169", family = "Duergar", size = Medium }
    , { name = "Duergar Sharpshooter", level = 0, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=168", family = "Duergar", size = Medium }
    , { name = "Duergar Taskmaster", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=170", family = "Duergar", size = Medium }
    , { name = "Dullahan", level = 7, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=171", family = "—", size = Medium }
    , { name = "Duneshaker Solifugid", level = 18, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=810", family = "Solifugid", size = Gargantuan }
    , { name = "Duskwalker", level = 4, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=332", family = "Planar Scion", size = Medium }
    , { name = "Dust Mephit", level = 1, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=659", family = "Elemental, Mephit", size = Small }
    , { name = "Dweomercat", level = 7, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=643", family = "—", size = Medium }
    , { name = "D'ziriak", level = 3, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=644", family = "—", size = Medium }
    , { name = "Eagle", level = -1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=172", family = "Eagle", size = Small }
    , { name = "Earth Mephit", level = 1, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=193", family = "Elemental, Mephit", size = Small }
    , { name = "Earthen Destrier", level = 4, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=651", family = "Elemental, Earth", size = Large }
    , { name = "Eberark", level = 10, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=1000", family = "—", size = Huge }
    , { name = "Efreeti", level = 9, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=214", family = "Genie", size = Large }
    , { name = "Elananx", level = 6, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=176", family = "—", size = Medium }
    , { name = "Elasmosaurus", level = 7, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=645", family = "—", size = Huge }
    , { name = "Elder Wyrmwraith", level = 23, creatureType = DragonUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=497", family = "Wyrmwraith", size = Gargantuan }
    , { name = "Electric Eel", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=174", family = "Eel", size = Small }
    , { name = "Elemental Avalanche", level = 11, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=186", family = "Elemental, Earth", size = Huge }
    , { name = "Elemental Hurricane", level = 11, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=181", family = "Elemental, Air", size = Huge }
    , { name = "Elemental Inferno", level = 11, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=191", family = "Elemental, Fire", size = Huge }
    , { name = "Elemental Tsunami", level = 11, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=200", family = "Elemental, Water", size = Huge }
    , { name = "Elemental Vessel, Water", level = 16, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=1009", family = "Elemental, Water", size = Large }
    , { name = "Elephant", level = 7, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=201", family = "Elephant", size = Huge }
    , { name = "Eloko", level = 7, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=443", family = "Biloko", size = Small }
    , { name = "Elysian Titan", level = 21, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=979", family = "Titan", size = Gargantuan }
    , { name = "Ember Fox", level = 2, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=655", family = "Elemental, Fire", size = Small }
    , { name = "Emperor Bird", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=434", family = "—", size = Medium }
    , { name = "Emperor Cobra", level = 5, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=808", family = "Snake", size = Large }
    , { name = "Eremite", level = 20, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=852", family = "Velstrac", size = Medium }
    , { name = "Erinys (Fury Devil)", level = 8, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=111", family = "Devil", size = Medium }
    , { name = "Esobok", level = 3, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=767", family = "Psychopomp", size = Medium }
    , { name = "Ether Spider", level = 5, creatureType = BeastEthereal, source = "https://2e.aonprd.com/Monsters.aspx?ID=203", family = "—", size = Large }
    , { name = "Ettin", level = 6, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=204", family = "—", size = Large }
    , { name = "Evangelist", level = 6, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=849", family = "Velstrac", size = Medium }
    , { name = "Excorion", level = 7, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=992", family = "—", size = Medium }
    , { name = "Faceless Butcher", level = 11, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=530", family = "—", size = Medium }
    , { name = "Faceless Stalker (Ugothol)", level = 4, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=4", family = "Alghollthu", size = Medium }
    , { name = "Faerie Dragon", level = 2, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=205", family = "—", size = Tiny }
    , { name = "Fen Mosquito Swarm", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=732", family = "Mosquito", size = Large }
    , { name = "Fetchling Scout", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=669", family = "Fetchling", size = Medium }
    , { name = "Filth Fire", level = 4, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=656", family = "Elemental, Fire", size = Medium }
    , { name = "Fire Giant", level = 10, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=223", family = "Giant", size = Large }
    , { name = "Fire Jellyfish Swarm", level = 6, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=706", family = "Jellyfish", size = Large }
    , { name = "Fire Mephit", level = 1, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=194", family = "Elemental, Mephit", size = Small }
    , { name = "Fire Yai", level = 14, creatureType = FiendGiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=748", family = "Oni", size = Large }
    , { name = "Firewyrm", level = 9, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=190", family = "Elemental, Fire", size = Huge }
    , { name = "Fjord Linnorm", level = 16, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=721", family = "Linnorm", size = Gargantuan }
    , { name = "Flame Drake", level = 5, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=160", family = "Drake", size = Large }
    , { name = "Flash Beetle", level = -1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=49", family = "Beetle", size = Small }
    , { name = "Flea Swarm", level = 5, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=502", family = "Flea", size = Large }
    , { name = "Flesh Golem", level = 8, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=238", family = "Golem", size = Large }
    , { name = "Flytrap Leshy", level = 4, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=717", family = "Leshy", size = Small }
    , { name = "Forge-Spurned", level = 5, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=473", family = "—", size = Medium }
    , { name = "Froghemoth", level = 13, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=676", family = "—", size = Huge }
    , { name = "Frost Drake", level = 7, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=163", family = "Drake", size = Large }
    , { name = "Frost Giant", level = 9, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=222", family = "Giant", size = Large }
    , { name = "Frost Troll", level = 4, creatureType = Giant, source = "https://2e.aonprd.com/Monsters.aspx?ID=831", family = "Troll", size = Large }
    , { name = "Frost Worm", level = 12, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=677", family = "—", size = Huge }
    , { name = "Frost Yai", level = 13, creatureType = FiendGiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=747", family = "Oni", size = Large }
    , { name = "Fuming Sludge", level = 7, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=984", family = "—", size = Medium }
    , { name = "Fungus Leshy", level = 2, creatureType = Fungus, source = "https://2e.aonprd.com/Monsters.aspx?ID=281", family = "Leshy", size = Small }
    , { name = "Gahlepod (Juvenile Brughadatch)", level = 7, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=527", family = "Brughadatch", size = Small }
    , { name = "Gancanagh (Passion Azata)", level = 4, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=35", family = "Azata", size = Medium }
    , { name = "Gargoyle", level = 4, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=210", family = "—", size = Medium }
    , { name = "Gashadokuro", level = 13, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=474", family = "—", size = Huge }
    , { name = "Gelatinous Cube", level = 3, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=321", family = "Ooze", size = Large }
    , { name = "Gelugon (Ice Devil)", level = 13, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=113", family = "Devil", size = Large }
    , { name = "Ghaele (Crusader Azata)", level = 13, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=37", family = "Azata", size = Medium }
    , { name = "Ghast", level = 2, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=219", family = "Ghoul", size = Medium }
    , { name = "Ghonhatine", level = 10, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=671", family = "Fleshwarp", size = Large }
    , { name = "Ghost Commoner", level = 4, creatureType = SpiritUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=216", family = "Ghost", size = Medium }
    , { name = "Ghost Mage", level = 10, creatureType = SpiritUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=217", family = "Ghost", size = Medium }
    , { name = "Ghoul", level = 1, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=218", family = "Ghoul", size = Medium }
    , { name = "Giant Amoeba", level = 1, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=751", family = "Ooze", size = Small }
    , { name = "Giant Anaconda", level = 8, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=382", family = "Snake", size = Huge }
    , { name = "Giant Animated Statue", level = 7, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=22", family = "Animated Object", size = Huge }
    , { name = "Giant Ant", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=548", family = "Ant", size = Medium }
    , { name = "Giant Aukashungi", level = 14, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=526", family = "Aukashungi", size = Huge }
    , { name = "Giant Badger", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=562", family = "Badger", size = Medium }
    , { name = "Giant Bat", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=46", family = "Bat", size = Large }
    , { name = "Giant Bone Skipper", level = 7, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=990", family = "Bone Skipper", size = Large }
    , { name = "Giant Centipede", level = -1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=77", family = "Centipede", size = Medium }
    , { name = "Giant Chameleon", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=724", family = "Lizard", size = Large }
    , { name = "Giant Cockroach", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=585", family = "Cockroach", size = Small }
    , { name = "Giant Crab", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=588", family = "Crab", size = Medium }
    , { name = "Giant Crawling Hand", level = 5, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=590", family = "Crawling Hand", size = Medium }
    , { name = "Giant Dragonfly Nymph", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=637", family = "Dragonfly", size = Small }
    , { name = "Giant Dragonfly", level = 4, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=638", family = "Dragonfly", size = Medium }
    , { name = "Giant Eagle", level = 3, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=173", family = "Eagle", size = Large }
    , { name = "Giant Flea", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=501", family = "Flea", size = Large }
    , { name = "Giant Fly", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=673", family = "Fly", size = Medium }
    , { name = "Giant Flytrap", level = 10, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=209", family = "Flytrap", size = Huge }
    , { name = "Giant Frilled Lizard", level = 5, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=290", family = "Lizard", size = Large }
    , { name = "Giant Frog", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=675", family = "Frog", size = Medium }
    , { name = "Giant Gecko", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=288", family = "Lizard", size = Medium }
    , { name = "Giant Hippocampus", level = 8, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=697", family = "Hippocampus", size = Huge }
    , { name = "Giant Jellyfish", level = 7, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=707", family = "Jellyfish", size = Large }
    , { name = "Giant Leech", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=711", family = "Leech", size = Medium }
    , { name = "Giant Maggot", level = 0, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=672", family = "Fly", size = Medium }
    , { name = "Giant Mantis", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=295", family = "Mantis, Giant", size = Large }
    , { name = "Giant Monitor Lizard", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=289", family = "Lizard", size = Medium }
    , { name = "Giant Moray Eel", level = 5, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=175", family = "Eel", size = Large }
    , { name = "Giant Mosquito", level = 6, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=733", family = "Mosquito", size = Medium }
    , { name = "Giant Octopus", level = 8, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=315", family = "Octopus", size = Huge }
    , { name = "Giant Rat", level = -1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=346", family = "Rat", size = Small }
    , { name = "Giant Scorpion", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=356", family = "Scorpion", size = Large }
    , { name = "Giant Snapping Turtle", level = 9, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=837", family = "Turtle", size = Gargantuan }
    , { name = "Giant Solifugid", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=809", family = "Solifugid", size = Large }
    , { name = "Giant Squid", level = 9, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=820", family = "Squid", size = Huge }
    , { name = "Giant Stag Beetle", level = 4, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=50", family = "Beetle", size = Large }
    , { name = "Giant Tarantula", level = 6, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=387", family = "Spider", size = Large }
    , { name = "Giant Tick", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=826", family = "Tick", size = Small }
    , { name = "Giant Toad", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=828", family = "Toad", size = Large }
    , { name = "Giant Viper", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=381", family = "Snake", size = Medium }
    , { name = "Giant Wasp", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=405", family = "Wasp", size = Large }
    , { name = "Giant Whiptail Centipede", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=581", family = "Centipede", size = Huge }
    , { name = "Giant Wolverine", level = 4, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=863", family = "Wolverine", size = Large }
    , { name = "Gibbering Mouther", level = 5, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=227", family = "—", size = Medium }
    , { name = "Gimmerling", level = 12, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=228", family = "—", size = Small }
    , { name = "Glabrezu (Treachery Demon)", level = 13, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=101", family = "Demon", size = Huge }
    , { name = "Glass Golem", level = 8, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=685", family = "Golem", size = Large }
    , { name = "Gnoll Cultist", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=230", family = "Gnoll", size = Medium }
    , { name = "Gnoll Hunter", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=229", family = "Gnoll", size = Medium }
    , { name = "Gnoll Sergeant", level = 4, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=231", family = "Gnoll", size = Medium }
    , { name = "Goblin Commando", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=233", family = "Goblin", size = Small }
    , { name = "Goblin Dog", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=236", family = "—", size = Medium }
    , { name = "Goblin Pyro", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=234", family = "Goblin", size = Small }
    , { name = "Goblin War Chanter", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=235", family = "Goblin", size = Small }
    , { name = "Goblin Warrior", level = -1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=232", family = "Goblin", size = Small }
    , { name = "Gogiteth", level = 12, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=237", family = "—", size = Large }
    , { name = "Goliath Spider", level = 11, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=388", family = "Spider", size = Gargantuan }
    , { name = "Gorgon", level = 8, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=686", family = "—", size = Large }
    , { name = "Gorilla", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=25", family = "Ape", size = Large }
    , { name = "Gosreg", level = 11, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=687", family = "—", size = Medium }
    , { name = "Gourd Leshy", level = 1, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=280", family = "Leshy", size = Small }
    , { name = "Granite Glyptodont", level = 8, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=654", family = "Elemental, Earth", size = Large }
    , { name = "Grauladon", level = 2, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=435", family = "—", size = Large }
    , { name = "Graveknight", level = 10, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=244", family = "—", size = Medium }
    , { name = "Graveshell", level = 1, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=436", family = "—", size = Large }
    , { name = "Gray Ooze", level = 4, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=753", family = "Ooze", size = Medium }
    , { name = "Great Cyclops", level = 12, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=87", family = "Cyclops", size = Huge }
    , { name = "Great White Shark", level = 4, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=365", family = "Shark", size = Huge }
    , { name = "Greater Barghest", level = 7, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=43", family = "Barghest", size = Large }
    , { name = "Greater Nightmare", level = 11, creatureType = BeastFiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=309", family = "Nightmare", size = Huge }
    , { name = "Greater Shadow", level = 7, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=363", family = "Shadow", size = Medium }
    , { name = "Green Hag", level = 4, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=255", family = "Hag", size = Medium }
    , { name = "Grendel", level = 19, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=690", family = "—", size = Large }
    , { name = "Griffon", level = 4, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=248", family = "—", size = Large }
    , { name = "Grig", level = 1, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=390", family = "Sprite", size = Tiny }
    , { name = "Grikkitog", level = 14, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=249", family = "—", size = Huge }
    , { name = "Grim Reaper", level = 21, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=250", family = "Grim Reaper", size = Medium }
    , { name = "Grimstalker", level = 5, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=691", family = "—", size = Medium }
    , { name = "Grindylow", level = 0, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=692", family = "—", size = Small }
    , { name = "Grippli Archer", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=448", family = "Grippli", size = Small }
    , { name = "Grippli Greenspeaker", level = 5, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=449", family = "Grippli", size = Small }
    , { name = "Grippli Scout", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=693", family = "Grippli", size = Small }
    , { name = "Grizzly Bear", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=47", family = "Bear", size = Large }
    , { name = "Grodair", level = 5, creatureType = BeastFey, source = "https://2e.aonprd.com/Monsters.aspx?ID=694", family = "—", size = Medium }
    , { name = "Grothlut", level = 3, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=206", family = "Fleshwarp", size = Medium }
    , { name = "Guard Dog", level = -1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=124", family = "Dog", size = Small }
    , { name = "Guardian Naga", level = 10, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=307", family = "Naga", size = Large }
    , { name = "Gug", level = 10, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=252", family = "—", size = Large }
    , { name = "Guthallath", level = 19, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=253", family = "—", size = Gargantuan }
    , { name = "Gylou (Handmaiden Devil)", level = 14, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=610", family = "Devil", size = Medium }
    , { name = "Hadrosaurid", level = 4, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=615", family = "Dinosaur", size = Huge }
    , { name = "Hamatula (Barbed Devil)", level = 11, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=609", family = "Devil", size = Medium }
    , { name = "Harpy", level = 5, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=258", family = "—", size = Medium }
    , { name = "Harrow Doll", level = 8, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=531", family = "—", size = Large }
    , { name = "Hegessik", level = 15, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=1019", family = "Protean", size = Large }
    , { name = "Hell Hound", level = 3, creatureType = BeastFiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=259", family = "Hell Hound", size = Medium }
    , { name = "Hellcat", level = 7, creatureType = BeastFiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=695", family = "—", size = Large }
    , { name = "Hellcrown", level = 1, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=437", family = "—", size = Tiny }
    , { name = "Herecite of Zevgavizeb", level = 10, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=520", family = "Herecite", size = Medium }
    , { name = "Hezrou (Toad Demon)", level = 11, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=601", family = "Demon", size = Large }
    , { name = "Hill Giant", level = 7, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=220", family = "Giant", size = Large }
    , { name = "Hippocampus", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=696", family = "Hippocampus", size = Large }
    , { name = "Hippogriff", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=698", family = "—", size = Large }
    , { name = "Hippopotamus", level = 5, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=699", family = "Hippopotamus", size = Large }
    , { name = "Hive Mother", level = 8, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=24", family = "Ankhrav", size = Huge }
    , { name = "Hobgoblin Archer", level = 4, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=262", family = "Hobgoblin", size = Medium }
    , { name = "Hobgoblin General", level = 6, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=263", family = "Hobgoblin", size = Medium }
    , { name = "Hobgoblin Soldier", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=261", family = "Hobgoblin", size = Medium }
    , { name = "Hodag", level = 6, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=701", family = "—", size = Large }
    , { name = "Homunculus", level = 0, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=264", family = "—", size = Tiny }
    , { name = "Horned Archon", level = 4, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=31", family = "Archon", size = Medium }
    , { name = "Hound Archon", level = 4, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=551", family = "Archon", size = Medium }
    , { name = "Hound Of Tindalos", level = 7, creatureType = AberrationTime, source = "https://2e.aonprd.com/Monsters.aspx?ID=702", family = "—", size = Medium }
    , { name = "Hunting Spider", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=386", family = "Spider", size = Medium }
    , { name = "Hyaenodon", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=271", family = "Hyena", size = Large }
    , { name = "Hydra", level = 6, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=269", family = "—", size = Huge }
    , { name = "Hyena", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=270", family = "Hyena", size = Medium }
    , { name = "Ice Golem", level = 5, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=683", family = "Golem", size = Medium }
    , { name = "Ice Linnorm", level = 17, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=285", family = "Linnorm", size = Gargantuan }
    , { name = "Ice Mephit", level = 1, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=660", family = "Elemental, Mephit", size = Small }
    , { name = "Icewyrm", level = 10, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=666", family = "Elemental, Water", size = Huge }
    , { name = "Icicle Snake", level = 2, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=663", family = "Elemental, Water", size = Small }
    , { name = "Iffdahsil", level = 21, creatureType = AberrationUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=876", family = "—", size = Gargantuan }
    , { name = "Ifrit", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=759", family = "Geniekin", size = Medium }
    , { name = "Iguanodon", level = 6, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=616", family = "Dinosaur", size = Huge }
    , { name = "Imentesh", level = 10, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=766", family = "Protean", size = Large }
    , { name = "Immortal Ichor", level = 15, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=483", family = "—", size = Medium }
    , { name = "Imp", level = 1, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=109", family = "Devil", size = Tiny }
    , { name = "Intellect Devourer", level = 8, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=703", family = "—", size = Small }
    , { name = "Interlocutor", level = 12, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=851", family = "Velstrac", size = Large }
    , { name = "Invidiak (Shadow Demon)", level = 7, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=599", family = "Demon", size = Medium }
    , { name = "Invisible Stalker", level = 7, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=179", family = "Elemental, Air", size = Medium }
    , { name = "Iridescent Animal", level = 7, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=510", family = "—", size = Huge }
    , { name = "Irlgaunt", level = 13, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=704", family = "—", size = Large }
    , { name = "Irnakurse", level = 9, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=670", family = "Fleshwarp", size = Large }
    , { name = "Iron Golem", level = 13, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=242", family = "Golem", size = Large }
    , { name = "Isqulug", level = 11, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=705", family = "—", size = Medium }
    , { name = "Izfiitar", level = 20, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=1020", family = "Protean", size = Medium }
    , { name = "Jabberwock", level = 23, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=652", family = "—", size = Huge }
    , { name = "Janni", level = 4, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=211", family = "Genie", size = Medium }
    , { name = "Jinkin", level = 1, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=247", family = "Gremlin", size = Tiny }
    , { name = "Jorogumo", level = 13, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=1005", family = "—", size = Medium }
    , { name = "Jotund Troll", level = 15, creatureType = Giant, source = "https://2e.aonprd.com/Monsters.aspx?ID=834", family = "Troll", size = Huge }
    , { name = "Jungle Drake", level = 6, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=161", family = "Drake", size = Large }
    , { name = "Jyoti", level = 9, creatureType = HumanoidPositive, source = "https://2e.aonprd.com/Monsters.aspx?ID=708", family = "—", size = Medium }
    , { name = "Kalavakus (Slaver Demon)", level = 10, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=455", family = "Demon", size = Medium }
    , { name = "Keketar", level = 17, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=338", family = "Protean", size = Large }
    , { name = "Kelpie", level = 4, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=709", family = "—", size = Large }
    , { name = "Kishi", level = 8, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=450", family = "—", size = Medium }
    , { name = "Kobold Dragon Mage", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=274", family = "Kobold", size = Small }
    , { name = "Kobold Scout", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=273", family = "Kobold", size = Small }
    , { name = "Kobold Warrior", level = -1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=272", family = "Kobold", size = Small }
    , { name = "Kolyarut", level = 12, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=10", family = "Aeon", size = Medium }
    , { name = "Korred", level = 4, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=710", family = "—", size = Small }
    , { name = "Kraken", level = 18, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=275", family = "—", size = Gargantuan }
    , { name = "Krooth", level = 8, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=276", family = "—", size = Large }
    , { name = "Kurnugian Jackal", level = 6, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=1024", family = "—", size = Medium }
    , { name = "Lamia Matriarch", level = 8, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=278", family = "Lamia", size = Large }
    , { name = "Lamia", level = 6, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=277", family = "Lamia", size = Large }
    , { name = "Lantern Archon", level = 1, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=30", family = "Archon", size = Small }
    , { name = "Leaf Leshy", level = 0, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=279", family = "Leshy", size = Small }
    , { name = "Legion Archon", level = 7, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=32", family = "Archon", size = Medium }
    , { name = "Lemure", level = 0, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=108", family = "Devil", size = Medium }
    , { name = "Leng Spider", level = 13, creatureType = AberrationDream, source = "https://2e.aonprd.com/Monsters.aspx?ID=713", family = "—", size = Huge }
    , { name = "Leopard", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=67", family = "Cat", size = Medium }
    , { name = "Leprechaun", level = 2, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=714", family = "—", size = Small }
    , { name = "Lerritan", level = 21, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=715", family = "—", size = Gargantuan }
    , { name = "Lesser Death", level = 16, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=251", family = "Grim Reaper", size = Medium }
    , { name = "Leucrotta", level = 5, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=718", family = "—", size = Large }
    , { name = "Leukodaemon (Pestilence Daemon)", level = 9, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=90", family = "Daemon", size = Large }
    , { name = "Leydroth", level = 17, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=719", family = "—", size = Large }
    , { name = "Lich", level = 12, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=282", family = "Lich", size = Medium }
    , { name = "Lillend (Muse Azata)", level = 7, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=36", family = "Azata", size = Large }
    , { name = "Lion Visitant", level = 5, creatureType = AnimalUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=513", family = "Visitant", size = Large }
    , { name = "Lion", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=68", family = "Cat", size = Large }
    , { name = "Living Boulder", level = 2, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=650", family = "Elemental, Earth", size = Small }
    , { name = "Living Graffiti", level = 3, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=503", family = "—", size = Medium }
    , { name = "Living Landslide", level = 5, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=183", family = "Elemental, Earth", size = Medium }
    , { name = "Living Sap", level = 6, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=451", family = "—", size = Medium }
    , { name = "Living Thunderclap", level = 4, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=647", family = "Elemental, Air", size = Medium }
    , { name = "Living Waterfall", level = 5, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=197", family = "Elemental, Water", size = Large }
    , { name = "Living Whirlwind", level = 5, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=178", family = "Elemental, Air", size = Medium }
    , { name = "Living Wildfire", level = 5, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=188", family = "Elemental, Fire", size = Medium }
    , { name = "Lizardfolk Defender", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=291", family = "Lizardfolk", size = Medium }
    , { name = "Lizardfolk Scout", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=292", family = "Lizardfolk", size = Medium }
    , { name = "Lizardfolk Stargazer", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=293", family = "Lizardfolk", size = Medium }
    , { name = "Luminous Ooze", level = 4, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=504", family = "—", size = Small }
    , { name = "Lunar Naga", level = 6, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=736", family = "Naga", size = Large }
    , { name = "Lurker In Light", level = 5, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=726", family = "—", size = Small }
    , { name = "Lusca", level = 17, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=1010", family = "—", size = Gargantuan }
    , { name = "Lyrakien (Wanderer Azata)", level = 1, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=34", family = "Azata", size = Tiny }
    , { name = "Magma Scorpion", level = 8, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=658", family = "Elemental, Fire", size = Large }
    , { name = "Mammoth", level = 10, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=202", family = "Elephant", size = Huge }
    , { name = "Mandragora", level = 4, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=727", family = "—", size = Small }
    , { name = "Manta Ray", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=786", family = "Ray", size = Large }
    , { name = "Manticore", level = 6, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=294", family = "—", size = Large }
    , { name = "Marid", level = 9, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=215", family = "Genie", size = Large }
    , { name = "Marilith (Pride Demon)", level = 17, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=103", family = "Demon", size = Large }
    , { name = "Marrmora", level = 15, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=728", family = "—", size = Medium }
    , { name = "Marsh Giant", level = 8, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=679", family = "Giant", size = Large }
    , { name = "Marut", level = 15, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=538", family = "Aeon", size = Large }
    , { name = "Mastodon", level = 9, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=668", family = "Elephant", size = Huge }
    , { name = "Mechanical Carny", level = 2, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=505", family = "—", size = Medium }
    , { name = "Medusa", level = 7, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=297", family = "—", size = Medium }
    , { name = "Megalania", level = 7, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=725", family = "Lizard", size = Huge }
    , { name = "Megalodon", level = 9, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=366", family = "Shark", size = Gargantuan }
    , { name = "Megaprimatus", level = 8, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=26", family = "Ape", size = Gargantuan }
    , { name = "Meladaemon (Famine Daemon)", level = 11, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=593", family = "Daemon", size = Large }
    , { name = "Melody On The Wind", level = 10, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=649", family = "Elemental, Air", size = Huge }
    , { name = "Merfolk Warrior", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=298", family = "Merfolk", size = Medium }
    , { name = "Merfolk Wavecaller", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=299", family = "Merfolk", size = Medium }
    , { name = "Mimic", level = 4, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=300", family = "—", size = Medium }
    , { name = "Minchgorm", level = 18, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=1011", family = "—", size = Huge }
    , { name = "Minotaur", level = 4, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=301", family = "—", size = Large }
    , { name = "Mist Stalker", level = 4, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=664", family = "Elemental, Water", size = Medium }
    , { name = "Mitflit", level = -1, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=245", family = "Gremlin", size = Small }
    , { name = "Mohrg", level = 8, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=729", family = "—", size = Medium }
    , { name = "Mokele-Mbembe", level = 9, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=452", family = "—", size = Huge }
    , { name = "Monadic Deva (Soul Angel)", level = 12, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=543", family = "Angel", size = Medium }
    , { name = "Moonflower", level = 8, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=730", family = "—", size = Huge }
    , { name = "Morlock", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=731", family = "—", size = Medium }
    , { name = "Morrigna", level = 15, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=340", family = "Psychopomp", size = Medium }
    , { name = "Movanic Deva (Guardian Angel)", level = 10, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=542", family = "Angel", size = Medium }
    , { name = "Mu Spore", level = 21, creatureType = Fungus, source = "https://2e.aonprd.com/Monsters.aspx?ID=302", family = "—", size = Gargantuan }
    , { name = "Mudwretch", level = 2, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=734", family = "—", size = Medium }
    , { name = "Mukradi", level = 15, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=303", family = "—", size = Gargantuan }
    , { name = "Mummy Guardian", level = 6, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=304", family = "Mummy", size = Medium }
    , { name = "Mummy Pharaoh", level = 9, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=305", family = "Mummy", size = Medium }
    , { name = "Muse Phantom", level = 5, creatureType = SpiritUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=511", family = "—", size = Medium }
    , { name = "Muurfeli", level = 16, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=873", family = "Blightburn Genies", size = Large }
    , { name = "Myrucarx", level = 18, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=1012", family = "—", size = Large }
    , { name = "Nabasu (Gluttony Demon)", level = 8, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=600", family = "Demon", size = Medium }
    , { name = "Naiad Queen", level = 7, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=313", family = "Nymph", size = Medium }
    , { name = "Naiad", level = 1, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=311", family = "Nymph", size = Medium }
    , { name = "Najra Lizard", level = 4, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=993", family = "—", size = Tiny }
    , { name = "Nalfeshnee (Boar Demon)", level = 14, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=602", family = "Demon", size = Huge }
    , { name = "Naunet", level = 7, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=337", family = "Protean", size = Large }
    , { name = "Necrophidius", level = 3, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=738", family = "—", size = Medium }
    , { name = "Nenchuuj", level = 19, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=1023", family = "Sahkil", size = Medium }
    , { name = "Neothelid", level = 15, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=739", family = "—", size = Gargantuan }
    , { name = "Nereid", level = 10, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=740", family = "—", size = Medium }
    , { name = "Nessian Warhound", level = 9, creatureType = BeastFiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=260", family = "Hell Hound", size = Large }
    , { name = "Night Hag", level = 9, creatureType = FiendHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=257", family = "Hag", size = Medium }
    , { name = "Nightmare", level = 6, creatureType = BeastFiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=308", family = "Nightmare", size = Large }
    , { name = "Nilith", level = 10, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=310", family = "—", size = Medium }
    , { name = "Nixie", level = 1, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=741", family = "—", size = Small }
    , { name = "Norn", level = 20, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=742", family = "—", size = Large }
    , { name = "Nosoi", level = 1, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=339", family = "Psychopomp", size = Tiny }
    , { name = "Nuckelavee", level = 9, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=743", family = "—", size = Large }
    , { name = "Nuglub", level = 2, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=689", family = "Gremlin", size = Small }
    , { name = "Nyogoth", level = 10, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=774", family = "Qlippoth", size = Medium }
    , { name = "Obrousian", level = 14, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=1013", family = "—", size = Medium }
    , { name = "Obsidian Golem", level = 16, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=875", family = "Golem", size = Large }
    , { name = "Ochre Jelly", level = 5, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=322", family = "Ooze", size = Large }
    , { name = "Ofalth", level = 10, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=316", family = "—", size = Large }
    , { name = "Ogre Boss", level = 7, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=319", family = "Ogre", size = Large }
    , { name = "Ogre Glutton", level = 4, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=318", family = "Ogre", size = Large }
    , { name = "Ogre Spider", level = 5, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=814", family = "Spider", size = Huge }
    , { name = "Ogre Warrior", level = 3, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=317", family = "Ogre", size = Large }
    , { name = "Olethrodaemon (Apocalypse Daemon)", level = 20, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=597", family = "Daemon", size = Gargantuan }
    , { name = "Onidoshi", level = 8, creatureType = FiendGiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=746", family = "Oni", size = Large }
    , { name = "Ooze Mephit", level = 1, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=661", family = "Elemental, Mephit", size = Small }
    , { name = "Orc Brute", level = 0, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=324", family = "Orc", size = Medium }
    , { name = "Orc Warchief", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=326", family = "Orc", size = Medium }
    , { name = "Orc Warrior", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=325", family = "Orc", size = Medium }
    , { name = "Orca", level = 5, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=619", family = "Dolphin", size = Huge }
    , { name = "Oread", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=760", family = "Geniekin", size = Medium }
    , { name = "Ostiarius", level = 5, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=848", family = "Velstrac", size = Medium }
    , { name = "Osyluth (Bone Devil)", level = 9, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=608", family = "Devil", size = Large }
    , { name = "Otyugh", level = 4, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=327", family = "—", size = Large }
    , { name = "Owlbear", level = 4, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=328", family = "—", size = Large }
    , { name = "Pachycephalosaurus", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=614", family = "Dinosaur", size = Large }
    , { name = "Pegasus", level = 3, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=329", family = "—", size = Large }
    , { name = "Peluda", level = 10, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=756", family = "—", size = Large }
    , { name = "Penqual", level = 15, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=1022", family = "Sahkil", size = Huge }
    , { name = "Peryton", level = 4, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=757", family = "—", size = Medium }
    , { name = "Petitioner", level = 1, creatureType = Petitioner, source = "https://2e.aonprd.com/Monsters.aspx?ID=758", family = "—", size = Medium }
    , { name = "Phistophilus (Contract Devil)", level = 10, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=112", family = "Devil", size = Medium }
    , { name = "Phoenix", level = 15, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=330", family = "—", size = Gargantuan }
    , { name = "Piscodaemon (Venom Daemon)", level = 10, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=592", family = "Daemon", size = Medium }
    , { name = "Pit Fiend (Tyrant Devil)", level = 20, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=114", family = "Devil", size = Large }
    , { name = "Pixie", level = 4, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=391", family = "Sprite", size = Small }
    , { name = "Plague Zombie", level = 1, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=424", family = "Zombie", size = Medium }
    , { name = "Planetar (Justice Angel)", level = 16, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=544", family = "Angel", size = Large }
    , { name = "Pleroma", level = 20, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=11", family = "Aeon", size = Large }
    , { name = "Polar Bear", level = 5, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=567", family = "Bear", size = Large }
    , { name = "Poltergeist", level = 5, creatureType = SpiritUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=334", family = "—", size = Medium }
    , { name = "Poracha", level = 4, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=335", family = "—", size = Medium }
    , { name = "Precentor", level = 16, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=463", family = "Velstrac", size = Medium }
    , { name = "Pteranodon", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=341", family = "Pterosaur", size = Large }
    , { name = "Pugwampi", level = 0, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=246", family = "Gremlin", size = Tiny }
    , { name = "Purple Worm", level = 13, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=73", family = "Cave Worm", size = Gargantuan }
    , { name = "Purrodaemon (War Daemon)", level = 18, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=596", family = "Daemon", size = Large }
    , { name = "Quasit", level = 1, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=97", family = "Demon", size = Tiny }
    , { name = "Quatoid", level = 7, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=198", family = "Elemental, Water", size = Small }
    , { name = "Quelaunt", level = 15, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=343", family = "—", size = Large }
    , { name = "Quetz Couatl", level = 10, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=587", family = "Couatl", size = Large }
    , { name = "Quetzalcoatlus", level = 7, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=342", family = "Pterosaur", size = Huge }
    , { name = "Quickling", level = 3, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=778", family = "—", size = Small }
    , { name = "Quoppopak", level = 11, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=779", family = "—", size = Large }
    , { name = "Qurashith", level = 17, creatureType = AberrationFiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=532", family = "—", size = Huge }
    , { name = "Radiant Warden", level = 17, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=780", family = "—", size = Gargantuan }
    , { name = "Raja Rakshasa", level = 10, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=345", family = "Rakshasa", size = Medium }
    , { name = "Rat Swarm", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=347", family = "Rat", size = Large }
    , { name = "Ratfolk Grenadier", level = 4, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=348", family = "Ratfolk", size = Small }
    , { name = "Raven Swarm", level = 3, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=782", family = "Raven", size = Large }
    , { name = "Ravener Husk", level = 14, creatureType = DragonUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=784", family = "Ravener", size = Gargantuan }
    , { name = "Ravener", level = 21, creatureType = DragonUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=783", family = "Ravener", size = Gargantuan }
    , { name = "Ravenile", level = 14, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=1002", family = "—", size = Large }
    , { name = "Raven", level = -1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=781", family = "Raven", size = Tiny }
    , { name = "Redcap", level = 5, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=349", family = "—", size = Small }
    , { name = "Reef Octopus", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=745", family = "Octopus", size = Small }
    , { name = "Reefclaw", level = 1, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=350", family = "—", size = Small }
    , { name = "Remnant of Barzillai", level = 10, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=458", family = "—", size = Medium }
    , { name = "Remorhaz", level = 7, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=351", family = "—", size = Huge }
    , { name = "Revenant", level = 6, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=787", family = "—", size = Medium }
    , { name = "Rhevanna", level = 22, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=1021", family = "—", size = Large }
    , { name = "Rhinoceros", level = 4, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=788", family = "Rhinoceros", size = Large }
    , { name = "Riding Dog", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=125", family = "Dog", size = Medium }
    , { name = "Riding Horse", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=266", family = "Horse", size = Large }
    , { name = "Riding Pony", level = 0, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=265", family = "Horse", size = Medium }
    , { name = "River Drake", level = 3, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=159", family = "Drake", size = Medium }
    , { name = "Roc", level = 9, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=352", family = "—", size = Gargantuan }
    , { name = "Roper", level = 10, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=353", family = "—", size = Large }
    , { name = "Rune Giant", level = 16, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=226", family = "Giant", size = Gargantuan }
    , { name = "Rusalka", level = 12, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=790", family = "—", size = Medium }
    , { name = "Rust Monster", level = 3, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=354", family = "—", size = Medium }
    , { name = "Sabosan", level = 5, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=453", family = "—", size = Medium }
    , { name = "Sacristan", level = 10, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=850", family = "Velstrac", size = Medium }
    , { name = "Salamander", level = 7, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=189", family = "Elemental, Fire", size = Medium }
    , { name = "Sand Sentry", level = 6, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=653", family = "Elemental, Earth", size = Medium }
    , { name = "Sandpoint Devil", level = 8, creatureType = BeastFiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=791", family = "—", size = Large }
    , { name = "Sard", level = 19, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=792", family = "—", size = Gargantuan }
    , { name = "Sarglagon (Drowning Devil)", level = 8, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=607", family = "Devil", size = Large }
    , { name = "Satyr", level = 4, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=355", family = "—", size = Medium }
    , { name = "Saurian Warmonger", level = 16, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=977", family = "Saurian", size = Huge }
    , { name = "Saurian Worldwatcher", level = 18, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=978", family = "Saurian", size = Huge }
    , { name = "Scalliwing", level = 3, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=1025", family = "Couatl", size = Tiny }
    , { name = "Scarecrow", level = 4, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=793", family = "—", size = Medium }
    , { name = "Sceaduinar", level = 7, creatureType = AberrationNegative, source = "https://2e.aonprd.com/Monsters.aspx?ID=794", family = "—", size = Medium }
    , { name = "Scorpion Swarm", level = 4, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=357", family = "Scorpion", size = Large }
    , { name = "Scythe Tree", level = 6, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=797", family = "—", size = Huge }
    , { name = "Sea Devil Baron", level = 6, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=360", family = "Sea Devil", size = Medium }
    , { name = "Sea Devil Brute", level = 4, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=359", family = "Sea Devil", size = Medium }
    , { name = "Sea Devil Scout", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=358", family = "Sea Devil", size = Medium }
    , { name = "Sea Drake", level = 6, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=641", family = "Drake", size = Large }
    , { name = "Sea Hag", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=254", family = "Hag", size = Medium }
    , { name = "Sea Serpent", level = 12, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=361", family = "—", size = Gargantuan }
    , { name = "Sea Snake", level = 0, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=807", family = "Snake", size = Small }
    , { name = "Sewer Ooze", level = 1, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=320", family = "Ooze", size = Medium }
    , { name = "Shadow Drake", level = 2, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=640", family = "Drake", size = Tiny }
    , { name = "Shadow Giant", level = 13, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=681", family = "Giant", size = Large }
    , { name = "Shadow", level = 4, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=362", family = "Shadow", size = Medium }
    , { name = "Shaitan", level = 7, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=213", family = "Genie", size = Large }
    , { name = "Shambler", level = 6, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=364", family = "—", size = Large }
    , { name = "Shatterling", level = 14, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=1006", family = "—", size = Small }
    , { name = "Shemhazian (Mutilation Demon)", level = 16, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=102", family = "Demon", size = Gargantuan }
    , { name = "Shield Archon", level = 10, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=33", family = "Archon", size = Large }
    , { name = "Shining Child", level = 12, creatureType = Astral, source = "https://2e.aonprd.com/Monsters.aspx?ID=367", family = "—", size = Medium }
    , { name = "Shoal Linnorm", level = 15, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=720", family = "Linnorm", size = Gargantuan }
    , { name = "Shocker Lizard", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=802", family = "—", size = Small }
    , { name = "Shoggoth", level = 18, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=368", family = "—", size = Huge }
    , { name = "Shoggti", level = 7, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=773", family = "Qlippoth", size = Large }
    , { name = "Shoony Hierarch", level = 4, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=523", family = "Shoony", size = Small }
    , { name = "Shoony Militia Member", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=522", family = "Shoony", size = Small }
    , { name = "Shoony Tiller", level = 0, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=521", family = "Shoony", size = Small }
    , { name = "Shuln", level = 12, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=369", family = "—", size = Huge }
    , { name = "Siege Shard", level = 3, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=987", family = "—", size = Tiny }
    , { name = "Simurgh", level = 18, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=370", family = "—", size = Gargantuan }
    , { name = "Sinspawn", level = 2, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=371", family = "—", size = Medium }
    , { name = "Skaveling", level = 5, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=803", family = "—", size = Large }
    , { name = "Skeletal Champion", level = 2, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=373", family = "Skeleton", size = Medium }
    , { name = "Skeletal Giant", level = 3, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=375", family = "Skeleton", size = Large }
    , { name = "Skeletal Horse", level = 2, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=374", family = "Skeleton", size = Large }
    , { name = "Skeletal Hulk", level = 7, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=376", family = "Skeleton", size = Huge }
    , { name = "Skeleton Guard", level = -1, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=372", family = "Skeleton", size = Medium }
    , { name = "Skinstitch", level = 5, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=994", family = "—", size = Large }
    , { name = "Skrik Nettle", level = 6, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=804", family = "—", size = Large }
    , { name = "Skulk", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=805", family = "—", size = Medium }
    , { name = "Skulltaker", level = 18, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=377", family = "—", size = Huge }
    , { name = "Skum (Ulat-Kini)", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=3", family = "Alghollthu", size = Medium }
    , { name = "Slime Mold", level = 2, creatureType = FungusOoze, source = "https://2e.aonprd.com/Monsters.aspx?ID=752", family = "Ooze", size = Large }
    , { name = "Slug", level = 8, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=806", family = "—", size = Huge }
    , { name = "Slurk", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=378", family = "—", size = Medium }
    , { name = "Smilodon", level = 6, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=70", family = "Cat", size = Large }
    , { name = "Snapping Flytrap", level = 3, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=208", family = "Flytrap", size = Large }
    , { name = "Snapping Turtle", level = -1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=836", family = "Turtle", size = Tiny }
    , { name = "Sod Hound", level = 3, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=182", family = "Elemental, Earth", size = Small }
    , { name = "Solar (Archangel)", level = 23, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=545", family = "Angel", size = Large }
    , { name = "Sordesdaemon (Pollution Daemon)", level = 15, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=1017", family = "Daemon", size = Large }
    , { name = "Soul Eater", level = 7, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=811", family = "—", size = Medium }
    , { name = "Soulbound Doll", level = 2, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=383", family = "—", size = Tiny }
    , { name = "Soulbound Ruin", level = 15, creatureType = ConstructUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=475", family = "—", size = Gargantuan }
    , { name = "Spark Bat", level = 2, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=646", family = "Elemental, Air", size = Tiny }
    , { name = "Spear Frog", level = 0, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=674", family = "Frog", size = Tiny }
    , { name = "Specter", level = 7, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=812", family = "—", size = Medium }
    , { name = "Sphinx", level = 8, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=384", family = "—", size = Large }
    , { name = "Spider Swarm", level = 0, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=385", family = "Spider", size = Large }
    , { name = "Spinosaurus", level = 11, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=617", family = "Dinosaur", size = Gargantuan }
    , { name = "Spiral Centurion", level = 11, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=815", family = "—", size = Medium }
    , { name = "Spirit Naga", level = 9, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=737", family = "Naga", size = Large }
    , { name = "Spiritbound Aluum", level = 16, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=477", family = "Aluum", size = Large }
    , { name = "Sportlebore Swarm", level = 7, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=816", family = "Sportlebore", size = Large }
    , { name = "Spriggan Bully", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=817", family = "Spriggan", size = Small }
    , { name = "Spriggan Warlord", level = 7, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=818", family = "Spriggan", size = Small }
    , { name = "Sprite", level = -1, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=389", family = "Sprite", size = Tiny }
    , { name = "Star Archon", level = 19, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=553", family = "Archon", size = Medium }
    , { name = "Steam Mephit", level = 1, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=662", family = "Elemental, Mephit", size = Small }
    , { name = "Stegosaurus", level = 7, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=120", family = "Dinosaur", size = Huge }
    , { name = "Stingray", level = 0, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=785", family = "Ray", size = Medium }
    , { name = "Stinkweed Shambler", level = 2, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=1026", family = "—", size = Small }
    , { name = "Stone Giant", level = 8, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=221", family = "Giant", size = Large }
    , { name = "Stone Golem", level = 11, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=241", family = "Golem", size = Large }
    , { name = "Stone Mauler", level = 9, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=185", family = "Elemental, Earth", size = Large }
    , { name = "Storm Giant", level = 13, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=225", family = "Giant", size = Huge }
    , { name = "Storm Lord", level = 9, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=180", family = "Elemental, Air", size = Large }
    , { name = "Striding Fire", level = 6, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=657", family = "Elemental, Fire", size = Medium }
    , { name = "Stygira", level = 7, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=821", family = "—", size = Medium }
    , { name = "Succubus (Lust Demon)", level = 7, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=98", family = "Demon", size = Medium }
    , { name = "Suli", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=761", family = "Geniekin", size = Medium }
    , { name = "Sunflower Leshy", level = 1, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=716", family = "Leshy", size = Small }
    , { name = "Svartalfar", level = 8, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=1003", family = "—", size = Medium }
    , { name = "Sylph", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=762", family = "Geniekin", size = Medium }
    , { name = "Taiga Giant", level = 12, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=680", family = "Giant", size = Huge }
    , { name = "Taiga Linnorm", level = 19, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=723", family = "Linnorm", size = Gargantuan }
    , { name = "Tallow Ooze", level = 11, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=533", family = "Ooze", size = Medium }
    , { name = "Tarn Linnorm", level = 20, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=286", family = "Linnorm", size = Gargantuan }
    , { name = "Tarrasque, The Armageddon Engine", level = 25, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=490", family = "Spawn of Rovagug", size = Gargantuan }
    , { name = "Tatzlwyrm", level = 2, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=822", family = "—", size = Medium }
    , { name = "Tendriculos", level = 7, creatureType = FungusPlant, source = "https://2e.aonprd.com/Monsters.aspx?ID=823", family = "—", size = Huge }
    , { name = "Tengu", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=392", family = "—", size = Medium }
    , { name = "Tenome", level = 4, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=995", family = "—", size = Medium }
    , { name = "Teraphant", level = 9, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=996", family = "—", size = Huge }
    , { name = "Terotricus", level = 19, creatureType = Fungus, source = "https://2e.aonprd.com/Monsters.aspx?ID=393", family = "—", size = Gargantuan }
    , { name = "Thanadaemon (Death Daemon)", level = 13, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=595", family = "Daemon", size = Medium }
    , { name = "Thanatotic Titan", level = 22, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=980", family = "Titan", size = Gargantuan }
    , { name = "The Stabbing Beast", level = 15, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=1007", family = "—", size = Huge }
    , { name = "Theletos", level = 7, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=536", family = "Aeon", size = Medium }
    , { name = "Thrasfyr", level = 17, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=824", family = "—", size = Huge }
    , { name = "Thulgant", level = 18, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=777", family = "Qlippoth", size = Large }
    , { name = "Thunderbird", level = 11, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=825", family = "—", size = Gargantuan }
    , { name = "Tick Swarm", level = 9, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=827", family = "Tick", size = Large }
    , { name = "Tidal Master", level = 9, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=199", family = "Elemental, Water", size = Large }
    , { name = "Tiefling", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=331", family = "Planar Scion", size = Medium }
    , { name = "Tiger", level = 4, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=69", family = "Cat", size = Large }
    , { name = "Titan Centipede", level = 9, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=582", family = "Centipede", size = Gargantuan }
    , { name = "Tixitog", level = 3, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=438", family = "—", size = Medium }
    , { name = "Tor Linnorm", level = 21, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=287", family = "Linnorm", size = Gargantuan }
    , { name = "Totenmaske", level = 7, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=829", family = "—", size = Medium }
    , { name = "Treerazer", level = 25, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=394", family = "—", size = Huge }
    , { name = "Triceratops", level = 8, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=121", family = "Dinosaur", size = Huge }
    , { name = "Triton", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=830", family = "—", size = Medium }
    , { name = "Troll King", level = 10, creatureType = Giant, source = "https://2e.aonprd.com/Monsters.aspx?ID=396", family = "Troll", size = Large }
    , { name = "Trollhound", level = 3, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=835", family = "—", size = Medium }
    , { name = "Troll", level = 5, creatureType = Giant, source = "https://2e.aonprd.com/Monsters.aspx?ID=395", family = "Troll", size = Large }
    , { name = "Trumpet Archon", level = 14, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=552", family = "Archon", size = Medium }
    , { name = "Twigjack", level = 3, creatureType = FeyPlant, source = "https://2e.aonprd.com/Monsters.aspx?ID=838", family = "—", size = Tiny }
    , { name = "Two-Headed Troll", level = 8, creatureType = Giant, source = "https://2e.aonprd.com/Monsters.aspx?ID=833", family = "Troll", size = Large }
    , { name = "Tyrannosaurus", level = 10, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=123", family = "Dinosaur", size = Gargantuan }
    , { name = "Tzitzimitl", level = 19, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=492", family = "—", size = Gargantuan }
    , { name = "Umonlee", level = 15, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=839", family = "—", size = Huge }
    , { name = "Undine", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=763", family = "Geniekin", size = Medium }
    , { name = "Unicorn", level = 3, creatureType = BeastFey, source = "https://2e.aonprd.com/Monsters.aspx?ID=397", family = "—", size = Large }
    , { name = "Unseen Servant", level = -1, creatureType = None, source = "https://2e.aonprd.com/Monsters.aspx?ID=1", family = "—", size = Medium }
    , { name = "Urdefhan Dominator", level = 14, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=879", family = "Urdefhan", size = Medium }
    , { name = "Urdefhan High Tormentor", level = 10, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=877", family = "Urdefhan", size = Medium }
    , { name = "Urdefhan Hunter", level = 12, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=878", family = "Urdefhan", size = Medium }
    , { name = "Urdefhan Tormentor", level = 5, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=841", family = "Urdefhan", size = Medium }
    , { name = "Urdefhan Warrior", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=840", family = "Urdefhan", size = Medium }
    , { name = "Uthul", level = 14, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=398", family = "—", size = Huge }
    , { name = "Vampire Bat Swarm", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=45", family = "Bat", size = Large }
    , { name = "Vampire Count", level = 6, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=400", family = "Vampire", size = Medium }
    , { name = "Vampire Mastermind", level = 9, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=401", family = "Vampire", size = Medium }
    , { name = "Vampire Spawn Rogue", level = 4, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=399", family = "Vampire", size = Medium }
    , { name = "Vampire Squid", level = 0, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=819", family = "Squid", size = Small }
    , { name = "Vampiric Mist", level = 3, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=845", family = "—", size = Medium }
    , { name = "Vanth", level = 7, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=769", family = "Psychopomp", size = Medium }
    , { name = "Vaspercham", level = 17, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=846", family = "—", size = Huge }
    , { name = "Vaultbreaker Ooze", level = 6, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=997", family = "Ooze", size = Large }
    , { name = "Vavakia", level = 18, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=976", family = "Demon", size = Huge }
    , { name = "Vazgorlu", level = 20, creatureType = AberrationAstral, source = "https://2e.aonprd.com/Monsters.aspx?ID=493", family = "—", size = Large }
    , { name = "Veiled Master (Vidileth)", level = 14, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=5", family = "Alghollthu", size = Large }
    , { name = "Velociraptor", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=117", family = "Dinosaur", size = Small }
    , { name = "Veranallia (Rebirth Azata)", level = 20, creatureType = Celestial, source = "https://2e.aonprd.com/Monsters.aspx?ID=560", family = "Azata", size = Medium }
    , { name = "Verdurous Ooze", level = 6, creatureType = Ooze, source = "https://2e.aonprd.com/Monsters.aspx?ID=754", family = "Ooze", size = Medium }
    , { name = "Vermlek (Worm Demon)", level = 3, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=500", family = "Demon", size = Medium }
    , { name = "Vexgit", level = 1, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=688", family = "Gremlin", size = Tiny }
    , { name = "Vine Lasher", level = 0, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=428", family = "Deadly Flora", size = Small }
    , { name = "Violet Fungus", level = 3, creatureType = Fungus, source = "https://2e.aonprd.com/Monsters.aspx?ID=853", family = "—", size = Medium }
    , { name = "Viper Vine", level = 13, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=854", family = "—", size = Large }
    , { name = "Viper", level = -1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=379", family = "Snake", size = Tiny }
    , { name = "Viskithrel", level = 15, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=981", family = "—", size = Huge }
    , { name = "Vitalia", level = 18, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=880", family = "—", size = Large }
    , { name = "Void Zombie", level = 1, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=855", family = "—", size = Medium }
    , { name = "Voidworm", level = 1, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=336", family = "Protean", size = Tiny }
    , { name = "Vrock (Wrath Demon)", level = 9, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=100", family = "Demon", size = Large }
    , { name = "Vrolikai (Death Demon)", level = 19, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=603", family = "Demon", size = Large }
    , { name = "Vrykolakas Ancient", level = 13, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=844", family = "Vampire, Vrykolakas", size = Medium }
    , { name = "Vrykolakas Master", level = 10, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=843", family = "Vampire, Vrykolakas", size = Medium }
    , { name = "Vrykolakas Spawn", level = 6, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=842", family = "Vampire, Vrykolakas", size = Medium }
    , { name = "War Horse", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=268", family = "Horse", size = Large }
    , { name = "War Pony", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=267", family = "Horse", size = Medium }
    , { name = "Warg", level = 2, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=402", family = "Warg", size = Medium }
    , { name = "Warsworn", level = 16, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=404", family = "—", size = Gargantuan }
    , { name = "Wasp Swarm", level = 4, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=406", family = "Wasp", size = Large }
    , { name = "Water Mephit", level = 1, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=195", family = "Elemental, Mephit", size = Small }
    , { name = "Water Orm", level = 10, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=856", family = "—", size = Huge }
    , { name = "Water Yai", level = 17, creatureType = FiendGiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=749", family = "Oni", size = Huge }
    , { name = "Web Lurker", level = 3, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=407", family = "—", size = Medium }
    , { name = "Wemmuth", level = 15, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=408", family = "—", size = Huge }
    , { name = "Wendigo", level = 17, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=409", family = "—", size = Large }
    , { name = "Werebear", level = 4, creatureType = BeastHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=412", family = "Werecreature", size = Large }
    , { name = "Wereboar", level = 2, creatureType = BeastHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=857", family = "Werecreature", size = Medium }
    , { name = "Wererat", level = 2, creatureType = BeastHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=410", family = "Werecreature", size = Medium }
    , { name = "Weretiger", level = 4, creatureType = BeastHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=858", family = "Werecreature", size = Medium }
    , { name = "Werewolf", level = 3, creatureType = BeastHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=411", family = "Werecreature", size = Medium }
    , { name = "Wight", level = 3, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=413", family = "Wight", size = Medium }
    , { name = "Will-o’-Wisp", level = 6, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=414", family = "—", size = Small }
    , { name = "Winter Wolf", level = 5, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=403", family = "Warg", size = Large }
    , { name = "Witchfire", level = 9, creatureType = SpiritUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=860", family = "—", size = Medium }
    , { name = "Witchwyrd", level = 6, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=861", family = "—", size = Medium }
    , { name = "Wolf", level = 1, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=415", family = "Wolf", size = Medium }
    , { name = "Wolverine", level = 2, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=862", family = "Wolverine", size = Medium }
    , { name = "Wood Giant", level = 6, creatureType = GiantHumanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=678", family = "Giant", size = Large }
    , { name = "Wood Golem", level = 6, creatureType = Construct, source = "https://2e.aonprd.com/Monsters.aspx?ID=684", family = "Golem", size = Medium }
    , { name = "Wooly Rhinoceros", level = 6, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=789", family = "Rhinoceros", size = Large }
    , { name = "Worm That Walks", level = 14, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=864", family = "Worm That Walks", size = Medium }
    , { name = "Wraith", level = 6, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=417", family = "Wraith", size = Medium }
    , { name = "Wyrmwraith", level = 17, creatureType = DragonUndead, source = "https://2e.aonprd.com/Monsters.aspx?ID=496", family = "Wyrmwraith", size = Gargantuan }
    , { name = "Wyvern", level = 6, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=162", family = "Drake", size = Large }
    , { name = "Xill", level = 6, creatureType = AberrationEthereal, source = "https://2e.aonprd.com/Monsters.aspx?ID=866", family = "—", size = Medium }
    , { name = "Xilvirek", level = 12, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=534", family = "—", size = Large }
    , { name = "Xorn", level = 7, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=184", family = "Elemental, Earth", size = Medium }
    , { name = "Xotani, The Firebleeder", level = 20, creatureType = Beast, source = "https://2e.aonprd.com/Monsters.aspx?ID=491", family = "Spawn of Rovagug", size = Gargantuan }
    , { name = "Xotanispawn", level = 17, creatureType = Animal, source = "https://2e.aonprd.com/Monsters.aspx?ID=487", family = "—", size = Large }
    , { name = "Xulgath Bilebearer", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=506", family = "Xulgath", size = Medium }
    , { name = "Xulgath Deepmouth", level = 12, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=881", family = "Xulgath", size = Medium }
    , { name = "Xulgath Gutrager", level = 10, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=535", family = "Xulgath", size = Medium }
    , { name = "Xulgath Leader", level = 3, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=420", family = "Xulgath", size = Medium }
    , { name = "Xulgath Skulker", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=419", family = "Xulgath", size = Medium }
    , { name = "Xulgath Spinesnapper", level = 5, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=514", family = "Xulgath", size = Large }
    , { name = "Xulgath Stoneliege", level = 8, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=524", family = "Xulgath", size = Medium }
    , { name = "Xulgath Thoughtmaw", level = 15, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=982", family = "Xulgath", size = Medium }
    , { name = "Xulgath Warrior", level = 1, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=418", family = "Xulgath", size = Medium }
    , { name = "Yaganty", level = 10, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=525", family = "—", size = Large }
    , { name = "Yamaraj", level = 20, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=770", family = "Psychopomp", size = Huge }
    , { name = "Yellow Musk Brute", level = 2, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=869", family = "Yellow Musk Creeper", size = Large }
    , { name = "Yellow Musk Creeper", level = 2, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=867", family = "Yellow Musk Creeper", size = Medium }
    , { name = "Yellow Musk Thrall", level = -1, creatureType = Plant, source = "https://2e.aonprd.com/Monsters.aspx?ID=868", family = "Yellow Musk Creeper", size = Medium }
    , { name = "Yeth Hound", level = 3, creatureType = BeastFiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=870", family = "—", size = Medium }
    , { name = "Yeti", level = 5, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=421", family = "—", size = Large }
    , { name = "Young Black Dragon", level = 7, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=127", family = "Dragon, Black", size = Large }
    , { name = "Young Blue Dragon", level = 9, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=130", family = "Dragon, Blue", size = Large }
    , { name = "Young Brass Dragon", level = 7, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=142", family = "Dragon, Brass", size = Large }
    , { name = "Young Brine Dragon", level = 8, creatureType = DragonElemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=621", family = "Dragon, Brine", size = Large }
    , { name = "Young Bronze Dragon", level = 9, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=145", family = "Dragon, Bronze", size = Large }
    , { name = "Young Cloud Dragon", level = 10, creatureType = DragonElemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=624", family = "Dragon, Cloud", size = Large }
    , { name = "Young Copper Dragon", level = 8, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=148", family = "Dragon, Copper", size = Large }
    , { name = "Young Crystal Dragon", level = 7, creatureType = DragonElemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=627", family = "Dragon, Crystal", size = Large }
    , { name = "Young Gold Dragon", level = 11, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=151", family = "Dragon, Gold", size = Large }
    , { name = "Young Green Dragon", level = 8, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=133", family = "Dragon, Green", size = Large }
    , { name = "Young Magma Dragon", level = 9, creatureType = DragonElemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=631", family = "Dragon, Magma", size = Large }
    , { name = "Young Red Dragon", level = 10, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=136", family = "Dragon, Red", size = Large }
    , { name = "Young Silver Dragon", level = 10, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=154", family = "Dragon, Silver", size = Large }
    , { name = "Young Umbral Dragon", level = 11, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=634", family = "Dragon, Umbral", size = Large }
    , { name = "Young White Dragon", level = 6, creatureType = Dragon, source = "https://2e.aonprd.com/Monsters.aspx?ID=139", family = "Dragon, White", size = Large }
    , { name = "Zaramuun", level = 16, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=422", family = "—", size = Large }
    , { name = "Zealborn", level = 12, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=1014", family = "—", size = Medium }
    , { name = "Zebub (Accuser Devil)", level = 3, creatureType = Fiend, source = "https://2e.aonprd.com/Monsters.aspx?ID=606", family = "Devil", size = Small }
    , { name = "Zelekhut", level = 9, creatureType = Monitor, source = "https://2e.aonprd.com/Monsters.aspx?ID=537", family = "Aeon", size = Large }
    , { name = "Zephyr Hawk", level = 3, creatureType = Elemental, source = "https://2e.aonprd.com/Monsters.aspx?ID=177", family = "Elemental, Air", size = Small }
    , { name = "Zombie Brute", level = 2, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=425", family = "Zombie", size = Large }
    , { name = "Zombie Hulk", level = 6, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=426", family = "Zombie", size = Huge }
    , { name = "Zombie Shambler", level = -1, creatureType = Undead, source = "https://2e.aonprd.com/Monsters.aspx?ID=423", family = "Zombie", size = Medium }
    , { name = "Zomok", level = 16, creatureType = DragonPlant, source = "https://2e.aonprd.com/Monsters.aspx?ID=871", family = "—", size = Gargantuan }
    , { name = "Zrukbat", level = 2, creatureType = Fey, source = "https://2e.aonprd.com/Monsters.aspx?ID=988", family = "—", size = Small }
    , { name = "Zuipnyrn (Moon Mole)", level = 3, creatureType = Aberration, source = "https://2e.aonprd.com/Monsters.aspx?ID=515", family = "—", size = Small }
    , { name = "Zyss Serpentfolk", level = 2, creatureType = Humanoid, source = "https://2e.aonprd.com/Monsters.aspx?ID=798", family = "Serpentfolk", size = Medium }
    ]


allCreaturesDict : Dict String Creature
allCreaturesDict =
    List.map (\c -> ( c.name, c )) allCreatures |> Dict.fromList
